<?php

$detail  = $CORE->getPageDetail();
$uriLinks= $detail['uriLinks'];
$curFile = '';
if ($detail['params']) {
	$curFile = $detail['params'][0];
}
if ($_POST) {
	$delete = array();
	foreach ($_POST as $key => $value) {
		if (false !== strpos($key, 'del_')) {
			$delete[str_replace('del_', '', $_POST[$key])] = '';
			continue;
		}
		if (false !== strpos($key, 'val_')) {
			continue;
		}
		if ($value && ! array_key_exists($key, $delete)) {
			$DATA['file'][$value] = $_POST['val_'.$key];
		}
	}
	$CORE->createConfigFile2($curFile, $DATA['file']);
	unset($_POST);
}

$files = $CORE->getProperties1('review');
if ( $curFile && empty($DATA['file'])) {
	$DATA['file'] = $CORE->getProperties1($curFile);
}
if ( ! array_key_exists('type', $PARAMETERS)) {
	return;
}
if ($PARAMETERS['type'] != 'configs') {
	return;
}

$path = $detail['path'];
foreach ($files as $key => $value) {
	$DATA['menuItems'][$key] = array(
		'title' => $value['_title'],
		'path'  => '',
	);
	if ( ! in_array($key, $detail['params'])) {
		$DATA['menuItems'][$key]['path'] = $path.'/'.$key;
	} else {
		$DATA['action'] = $path.'/'.$key;
	}
}?>