<link rel="stylesheet" type="text/css" href="<?=$templatePath?>css/css.css">
<div class = "h-menu-schMap">
	<nav>
		<ul>
			<?foreach ($DATA['menuItems'] as $key => $value) {
				echo '<li>';
				if($value['path']) {
					echo "<a href=\"{$value['path']}\">{$value['title']}</a>";
				} else {
					echo $value['title'].'</li>';
				}
			}?>
		</ul>
	</nav>
	<!-- image home-->
</div>

<?if ( empty($DATA['map']['arr'])) {
	return;
}?>

<div class = "schMap">
	<form action="<?=$DATA['other']['action']?>" method="POST">
		<?unset($DATA['other']['action']);
		foreach ($DATA['other'] as $key => $value) {
			if ( array_key_exists($key, $DATA['translate']) ){
				echo $DATA['translate'][$key];
			} else {
				echo $key;
			}
			echo ": <input type=\"text\" name=\"$key\" value=\"{$DATA['other'][$key]}\">";
		}?>
		<table>
			<?
			echo '<tr><td></td>';
			for ($i=0; $i < $DATA['map']['maxDepth'] + 1; $i++) { 
				echo "<td>Уровень $i</td>";
			}
			echo '<tr>';
			foreach ($DATA['map']['arr'] as $row => $unit) {
				echo '<tr>';
					for ($i=0; $i < $unit['depth']; $i++) { 
						echo '<td></td>';
					}
					echo '<td align="right"><input type="checkbox" name="'.$unit['cont']['_filename'].'_del" value="_del"></td>';
					echo '<td>';
					foreach ($unit['cont'] as $key => $value) {
						if ( array_key_exists($key, $DATA['translate']) ){
							echo $DATA['translate'][$key];
						} else {
							echo $key;
						}
						echo ': <input type="text" name="'.$unit['cont']['_filename'].$key.'" value="'.$value.'"><br>';
					}
					echo '<input type="hidden" name="'.$unit['cont']['_filename'].'_depth" value="'.$unit['depth'].'"></td>';
					for ($i=0; $i < ($DATA['map']['maxDepth'] - $unit['depth']); $i++) { 
						echo '<td></td>';
					}
				echo '</tr>';
			}?>
		</table>
		<input type="submit" name="accept" value="Отправить">
	</form>
</div>