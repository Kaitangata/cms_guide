<?php

$detail  = $CORE->getPageDetail();
$uriLinks= $detail['uriLinks'];
$path    = $detail['path'];
$curFile = '';

if ($_POST) {
	$rewriteScheme = false;
	$schDetail = array(
		'_filename'=> $_POST['_filename'],
		'_title'   => $_POST['_title'],
		'_theme'   => $_POST['_theme']
	);
	unset($_POST['_filename']);
	unset($_POST['_title']);
	unset($_POST['_theme']);
	if ( array_key_exists('accept', $_POST)) {
		unset($_POST['accept']);
		$rewriteScheme = true;
	}
	
	demap($_POST, $schema);
	foreach ($schDetail as $key => $value) {
		$schema[$key] = $value;
	}
	$CORE->makeSchema1($schema);
	if ($rewriteScheme) {
		echo 'Схема успешно перезаписана';
	}
	unset($schema);
}

if ($detail['params']) {
	$curFile = $detail['params'][0];
}
$files = $CORE->getSchemes1('review');
foreach ($files as $key => $value) {
	$DATA['menuItems'][$key] = array(
		'title' => $value['_title'],
		'path'  => '',
	);
	if ( $key == $curFile) {
		$DATA['other']['action']   = $path.'/'.$key;
		$DATA['other']['_filename']= $curFile;
		$DATA['other']['_title']   = $value['_title'];
		$DATA['other']['_theme']   = $value['_theme'];
	} else {
		$DATA['menuItems'][$key]['path'] = $path.'/'.$key;
	}
}

if ( $DATA['other']['_filename'] ) {
	$schema      = $CORE->getSchemes1($DATA['other']['_filename']);
	$DATA['map'] = makeMap2( $schema );
}
$dictionary = translateTableFields();
if ( is_array($dictionary) ){
	foreach ($dictionary as $word => $trans) {
		$DATA['translate']['_'.$word] = $trans;
	}
}

?>