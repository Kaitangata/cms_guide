<?php
$detail   = $CORE->getPageDetail();
$tables   = $CORE->getTable3();
if ( ! is_array($tables) ){
	return;
}
$tableName= '';
foreach ($tables as $key => $value) {
	$DATA['menu'][$value] = array(
		'path' => '',
	);
	if ( array_key_exists(0, $detail['params']) && $value == $detail['params'][0] ){
		$tableName      = $value;
		$DATA['action'] = $detail['path'].'/'.$value;
		continue;
	}
	$DATA['menu'][$value]['path'] = $detail['path'].'/'.$value;
}
if ( $_POST ){
	echo 'Данные обновлены';
	unset($_POST['update']);
	$deletedID = array();
	foreach ($_POST as $key => $value) {
		if ( false !== strpos($key, 'del_') ){
			$deletedID[] = $value;
			unset($_POST[$key]);
		}
	}
	$ignore = -1;
	foreach ($_POST as $key => $value) {
		$field_id = explode('_', $key);
		$id    = $field_id[1];
		$field = $field_id[0];
		if ( $ignore == $id ){
			continue;
		}
		if ( in_array($id, $deletedID) ){
			$ignore = $id;
			$general[$id] = array();
			continue;
		}
		if ( false !== strpos($id, 'new') ){
			$new[str_replace('new', '', $id)][$field] = $value;
		} else {
			$general[$id][$field] = $value;
		}
	}
	$table = $CORE->updateTable3($tableName, $general, $new);
}
if ( $tableName ){
	if ( ! isset($table) ) {
		$table = $CORE->getTable3($tableName, array(), true);
	}
	foreach ($table as $key => $value) {
		$DATA[$key] = $value;
	}
	if ( ! is_numeric($DATA['detail']['maxID']) ){
		$DATA['detail']['maxID'] = 0;
	}
}
?>