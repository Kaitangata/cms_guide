<link rel="stylesheet" type="text/css" href="<?=$templatePath?>css/css.css">

<?if ( array_key_exists('Error', $DATA) ){
	echo $DATA['Error'];
	return;
}
if ( empty($DATA['content']) ){
	return;
}
if ( array_key_exists('title', $DATA) ){
	echo "<p align=\"center\">{$DATA['title']}</p>";
}
?>
<div class = "tableWM">
	<form method="POST" action="<?=$DATA['action']?>">
		<table>
			<tr>
				<td>
					Удаление
				</td>
				<?foreach ($DATA['fields'] as $field => $value) {
					if ( ! in_array($field, $DATA['fhidden']) ){
						if ( array_key_exists($field, $DATA['translate']) ){
							echo '<td>'.$DATA['translate'][$field].'</td>';
						} else {
							echo '<td>'.$field.'</td>';
						}
					}
				}?>
			</tr>
			<?foreach ($DATA['content'] as $key => $row) {
				echo "<td><input type=\"checkbox\" name= \"del_$key\" value=\"$key\"></td>";
				foreach ($row as $field => $value) {
					if ( in_array($field, $DATA['fhidden']) ){
						echo "<input type=\"hidden\" name=\"{$field}|$key\" value=\"$value\">";
					} elseif ( $DATA['fields'][$field]['Type'] == 'Enum' ){
						echo "<td><select name=\"{$field}|$key\">
								<option value=\"$value\">$value</option>";
						foreach ($DATA['fields'][$field]['Values'] as $k => $v) {
							if ( $value != $v ){
								echo "<option value=\"$v\">$v</option>";
							}
							
						}
						echo '</select></td>';
					} elseif ( $DATA['fields'][$field]['Key'] == 'PRI' && isNumericTypeSQL($DATA['fields'][$field]['Type']) ){
						echo "<td><input type=\"hidden\" name=\"{$field}|$key\" value=\"$value\">$value</td>";
					} else {
						echo "<td><input type=\"text\" name=\"{$field}|$key\" value=\"$value\"></td>";
					}
				}
				echo '<tr>';
			}
			for($i = $DATA['detail']['maxID'] + 1; $i < $DATA['detail']['maxID'] + 4; $i++) {
				echo '<tr><td></td>';
				foreach ($DATA['fields'] as $field => $value) {
					if ( in_array($field, $DATA['fhidden']) ){
						if ( array_key_exists($field, $PARAMETERS['fields']) ){
							echo "<input type=\"hidden\" name=\"{$field}|new{$i}\" value=\"{$PARAMETERS['fields'][$field]}\">";
						}
					} elseif ($value['Type'] == 'Enum') {
						echo "<td><select name=\"{$field}|new{$i}\">
								<option value=\"{$value['Default']}\">{$value['Default']}</option>";
						foreach ($value['Values'] as $k => $v) {
							if ( $value['Default'] != $v ){
								echo "<option value=\"$v\">$v</option>";
							}
						}
						echo '</select></td>';
					} elseif ( $value['Key'] == 'PRI' && isNumericTypeSQL($DATA['fields'][$field]['Type']) ){
						echo "<td><input type=\"hidden\" name=\"{$field}|new{$i}\" value=\"$i\">$i</td>";
					} else {
						echo "<td><input type=\"text\" name=\"{$field}|new{$i}\" value=\"\"></td>";
					}
				}
				echo '<tr>';
			}?>
		</table>
		<input class="submit" type="submit" name="<?=$PARAMETERS['table']?>" value="Подтвердить изменения">
	</form>
</div>