<?php
$globalData = $CORE->getGlobalData1($PARAMETERS['data']);
if ( array_key_exists('path', $globalData) ){
	$DATA['action'] = $globalData['path'];
} else {
	$detal          = $CORE->getPageDetail();
	$DATA['action'] = $detal['fullPath'];
}
if ( array_key_exists('title', $PARAMETERS) ){
	$DATA['title'] = $PARAMETERS['title'];
}
if ( ! array_key_exists('data', $PARAMETERS) ){
	$PARAMETERS['data'] = '';
}
if ( ! array_key_exists('fields', $PARAMETERS) ){
	$PARAMETERS['fields'] = array();
}
if ( array_key_exists('fields', $globalData) ){
	foreach ($globalData['fields'] as $key => $value) {
		if( ! array_key_exists($key, $PARAMETERS['fields']) ){
			$PARAMETERS['fields'][$key] = $value;
		}
	}
}
if ( ! empty($PARAMETERS['fields']) ){
	foreach ($PARAMETERS['fields'] as $field => $value) {
		if ( $value == '' ){
			//$PARAMETERS['fields'] = array();
			return;
		}
	}
}
if ( ! array_key_exists('table', $PARAMETERS) ){
	$PARAMETERS['table'] = '';
}
if ( $PARAMETERS['table'] == '' ){
	if ( array_key_exists('table', $globalData) && $globalData['table'] != '' ){
		$PARAMETERS['table'] = $globalData['table'];
	} else {
		$DATA['Error'] = 'Для работы компонента ('.$CORE->getComponentName().') недостаточно данных о запрашиваемой таблице.';
		return;
	}
}
if ( array_key_exists('fhidden', $PARAMETERS) ){
	$DATA['fhidden'] = $PARAMETERS['fhidden'];
} else {
	$DATA['fhidden'] = array();
}
$DATA['translate'] = translateTableFields($PARAMETERS['table']);

if ( isset($_POST) && array_key_exists($PARAMETERS['table'], $_POST) ){
	echo 'Данные обновлены';
	unset($_POST[$PARAMETERS['table']]);
	$deletedID = array();
	foreach ($_POST as $key => $value) {
		if ( false !== strpos($key, 'del_') ){
			$deletedID[] = $value;
			unset($_POST[$key]);
		}
	}
	$ignore = -1;
	foreach ($_POST as $key => $value) {
		$field_id = explode('|', $key);
		$id    = $field_id[1];
		$field = $field_id[0];
		if ( $ignore == $id ){
			continue;
		}
		if ( in_array($id, $deletedID) ){
			$ignore = $id;
			$general[$id] = array();
			continue;
		}
		if ( false !== strpos($id, 'new') ){
			$new[str_replace('new', '', $id)][$field] = $value;
		} else {
			$general[$id][$field] = $value;
		}
	}
	foreach ($new as $key => $row) {
		$empty = true;
		foreach ($row as $field => $value) {
			if ( ! array_key_exists($field, $PARAMETERS['fields']) && ! empty($value) ){
				$empty = false;
				break;
			}
		}
		if ( $empty ){
			unset($new[$key]);
		}
	}
	$table = $CORE->updateTable3($PARAMETERS['table'], $general, $new);
	unset($_POST);
}

if ( ! isset($table) ) {
	$table = $CORE->getTable3(
		$PARAMETERS['table'], 
		array(
			'fields' => array(),
			'where'  => $PARAMETERS['fields']
		),
		true
	);
}

foreach ($table as $key => $value) {
	$DATA[$key] = $value;
}
?>