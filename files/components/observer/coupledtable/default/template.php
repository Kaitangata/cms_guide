<link rel="stylesheet" type="text/css" href="<?=$templatePath?>css/css.css">

<?if ( empty($DATA['content']) ){
	return;
}?>
<div class = "table">
	<form method="POST" action="<?=$DATA['action']?>">
		<table>
			<tr>
				<td>
					Удаление
				</td>
				<?foreach ($DATA['fields'] as $field => $value) {
					echo '<td>'.$field.'</td>';
				}?>
			</tr>
			<?foreach ($DATA['content'] as $key => $row) {
				if ( array_key_exists('error', $DATA) && in_array($key, $DATA['error']) ) {
					echo '<tr class="error1">';
				} else {
					echo '<tr>';
				}
				echo "<td><input type=\"checkbox\" name= \"del_$key\" value=\"$key\"></td>";
				foreach ($row as $field => $value) {
					if ( $DATA['fields'][$field]['Type'] == 'Enum' ){
						echo "<td><select name=\"{$field}_$key\">
								<option value=\"$value\">$value</option>";
						foreach ($DATA['fields'][$field]['Values'] as $k => $v) {
							if ( $value != $v ){
								echo "<option value=\"$v\">$v</option>";
							}
							
						}
						echo '</select></td>';
					} elseif ( $DATA['fields'][$field]['Key'] == 'PRI' && (strpos($DATA['fields'][$field]['Type'], 'int') !== false) ){
						echo "<td><input type=\"hidden\" name=\"{$field}_$key\" value=\"$value\">$value</td>";
					} else {
						echo "<td><input type=\"text\" name=\"{$field}_$key\" value=\"$value\"></td>";
					}
				}
				echo '<tr>';
			}
			for($i = $DATA['detail']['maxID'] + 1; $i < $DATA['detail']['maxID'] + 4; $i++) {
				echo '<tr><td></td>';
				foreach ($DATA['fields'] as $field => $value) {
					if ($value['Type'] == 'Enum') {
						echo "<td><select name=\"{$field}_new{$i}\">
								<option value=\"{$value['Default']}\">{$value['Default']}</option>";
						foreach ($value['Values'] as $k => $v) {
							if ( $value['Default'] != $v ){
								echo "<option value=\"$v\">$v</option>";
							}
							
						}
						echo '</select></td>';
					} elseif ( $value['Key'] == 'PRI' && (strpos($value['Type'], 'int') !== false) ){
						echo "<td><input type=\"hidden\" name=\"{$field}_new{$i}\" value=\"$i\">$i</td>";
					} else {
						echo "<td><input type=\"text\" name=\"{$field}_new{$i}\" value=\"\"></td>";
					}
				}
				echo '<tr>';
			}?>
		</table>
		<input class="submit" type="submit" name="update"   value="Подтвердить изменения">
	</form>
</div>