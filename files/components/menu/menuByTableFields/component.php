<?php

$detail = $CORE->getPageDetail();
$tables = $CORE->getTable3();
$params = array();
if ( ! is_array($tables) ){
	$DATA['Error'] = 'Ошибка загрузки таблиц.';
	return;
}
if ( ! array_key_exists('target', $PARAMETERS) ){
	$PARAMETERS['target'] = '';
}
if ( $PARAMETERS['target'] == '' ){
	$tablesMod = true;
	$menuName = 'Таблицы';
	$menu[$menuName] = $tables;
	$PARAMETERS['fields'] = array(
		'tables' => $menuName
	);
} else {
	if ( in_array($PARAMETERS['target'], $tables) ){
		if ( ! array_key_exists('fields', $PARAMETERS) || empty($PARAMETERS['fields']) ){
			$DATA['Error'] = 'Не переданы поля отбора.';
			return;
		}
		$fields = $CORE->getTableDesc1($PARAMETERS['target']);
		if ( ! is_array($fields) ){
			$DATA['Error'] = 'Ошибка загрузки описания таблицы:'.$PARAMETERS['target'].'.';
			return;
		}
		foreach ($PARAMETERS['fields'] as $key => $value) {
			if ( ! array_key_exists($key, $fields) ){
				$DATA['Error'] = 'Передан некорректный состав полей отбора.';
				return;
			}
		}
		foreach ($PARAMETERS['fields'] as $field => $title) {
			$values = $CORE->query1("SELECT DISTINCT `$field` FROM {$PARAMETERS['target']} ORDER BY `$field`;");
			foreach ($values as $key => $row) {
				$menu[$title][] = $row[$field];
			}
		}
	} else {
		$DATA['Error'] = 'Указанной таблицы не существует.';
		return;
	}
}
foreach ($PARAMETERS['fields'] as $key => $value) {
	$params[$value] = NULL;
}
foreach ($params as $key => $value) {
	if ( empty($detail['params']) ){
		break;
	}
	$params[$key] = array_shift($detail['params']);
}
$choised = '';
foreach ($params as $menuName => $choisedItem) {
	$DATA['menu'][$menuName] = $menu[$menuName];
	foreach ($menu[$menuName] as $key => $item) {
		if ( $item == $choisedItem ){
			$choised = true;
			$choiseArr[] = $item;
			$chItem  = $choisedItem;
			$DATA['uri'][$menuName][$key] = '';
			if ( isset($path) ){
				$curPath = $path;
				$path.='/'.$item;
			} else {
				$curPath= $detail['path'];
				$path   = $curPath.'/'.$item;
			}
		} elseif ( isset($path) && $path != '' ) {
			if ( $choised ){
				$DATA['uri'][$menuName][$key] = $curPath.'/'.$item;
			} else {
				$DATA['uri'][$menuName][$key] = $path.'/'.$item;
			}
		} else {
			$DATA['uri'][$menuName][$key] = $detail['path'].'/'.$item;
		}
		
	}
	if ( $choised ){
		$choised = false;
	} else {
		break;
	}
}

foreach ($PARAMETERS['fields'] as $fields => $title) {
	if ( ! empty($choiseArr) ){
		$PARAMETERS['fields'][$fields] = array_shift($choiseArr);
	} else {
		$PARAMETERS['fields'][$fields] = '';
	}
}

if ( $tablesMod ){
	$CORE->setGlobalData1(array(
		'table' => $chItem,
		'fields'=> array(),
		'path'  => $path
	));
} else {
	$CORE->setGlobalData1(array(
		'table' => $PARAMETERS['target'],
		'fields'=> $PARAMETERS['fields'],
		'path'  => $path
	));
}

?>