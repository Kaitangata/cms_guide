<?php

$detail  = $CORE->getPageDetail();
$sch     = $CORE->getSchema();
$uriLinks= $detail['uriLinks'];
$path    = '/'.$detail['schema'].'/';

if ( ! array_key_exists('type', $PARAMETERS) || empty($uriLinks)) {
		$PARAMETERS['type'] = 'general';
}

if ($PARAMETERS['type'] != 'general') { 
	foreach ($uriLinks as $key => $value) {
		if ($detail['page'] == $value) {
			if ($PARAMETERS['type'] == 'inner') {
				$sch = $sch[$value];
				$path.= $value.'/';
				break;
			}
		}
		if (array_key_exists($value, $sch)) {
			$sch = $sch[$value];
			$path.= $value.'/';
		}
	}
}

foreach ($sch as $key => $value) {
	if ($key{0} != '_') {
		$DATA['menuItems'][$key] = array(
			'title' => $value['_title'],
			'path'   => $path.$key.'/',
		);
	}
}
if (array_key_exists('menuItems', $DATA) ){
	$DATA['choised'] = $detail['page'];
}
?>