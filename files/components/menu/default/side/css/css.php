<?header("Content-type: text/css; charset: UTF-8")?>
.v-menu ul {
	list-style-image: url(<?=$templatePath?>../img/marker1.png);
}
.v-menu a {
	color: #5d5d5dff;
}
.v-menu li {
	margin-top: 3px; 
}
.line {
	width: 60%;
	height: 1px;
	background-color: #5d5d5dff;
	display: inline-block;
	margin-bottom: 4px; 
}
