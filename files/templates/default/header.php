<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="<?=$templatePath?>css/css.css">
	<title><?=$title?></title>
</head>
<body>
	<div class = "header">
		<div class = "profile-info">
			<img class = "profile-img" src = "<?=$templatePath?>img/user.png">
			<div>Гость</div>
			<img class = "profile-mon" src = "<?=$templatePath?>img/monogram.png">
		</div>
		<?$CORE->includeComponent4(
			'menu',
			'default',
			'default',
			array(
				'type' => 'general',
			)
		)?>
	</div>
	<div class = "main">
		<table>
			<tr>
				<td class = "sidebar">
					<div class = "sidebar-icon">
						<img src = "<?=$templatePath?>img/icon1.png">
						<p>Структура раздела</p>
						<div class = "line"></div>
					</div>
					<?$CORE->includeComponent4(
						'menu',
						'default',
						'side',
						array(
							'type' => 'inner',
						)
					)?>
				</td>
				<td class = "content">
					<div class="page">
						<div class = "page-title">
							<?=$title?>
						</div>