<?php
class Schema {
//private:
	private $currentSchema;
	private $data;
//public:
	public function makeSchema1($schema)
	{
		global $CORE;
		$path      = $CORE->getPath2('schemes');
		$filePath  = $path.$schema['_filename'].'.txt';
		$reviewPath= $path.'_review.txt';
		
		$reviewCont = unserialize(file_get_contents($reviewPath));
		$reviewCont[$schema['_filename']] = array(
			'_title' => $schema['_title'],
			'_theme' => $schema['_theme']
		);
		file_put_contents($reviewPath, serialize($reviewCont));

		file_put_contents($filePath, serialize($schema));
	}
	public function getSchemes1($type)
	{
		global $CORE;
		if($data['schema'] == $type){
			return $this->currentSchema;
		}
		$fullSchemPath = $CORE->getPath2('schemes');
		if ($type == 'review' && file_exists($fullSchemPath.'_review.txt')) {
			$fileContent = file_get_contents($fullSchemPath.'_review.txt');
			return unserialize($fileContent);
		}
		$schemFiles = array();
		if ($hdir = opendir($fullSchemPath)) {
			while(false !== ($schemFile = readdir($hdir)) )
			{
				if (false !== strpos($schemFile,'.txt') && $schemFile{0} != '_') {
					$schemFiles[basename($schemFile, '.txt')] = array();
				}
			}
			closedir($hdir);
		}
		if (array_key_exists($type, $schemFiles)) {
			$fileContent = file_get_contents($fullSchemPath.$type.'.txt');
			return unserialize($fileContent);
		}
		if ($type == 'all' || $type == 'review') {
			$review = array();
			foreach ($schemFiles as $key => &$value) {
				$fileContent = file_get_contents($fullSchemPath.$key.'.txt');
				$value       = unserialize($fileContent);
				foreach ($value as $k => $v) {
					if ( $k{0} == '_') {
						$review[$key][$k] = $v;
					}
				}
			}
			if ( ! file_exists($fullSchemPath.'_review.txt')) {
				$review = serialize($review);
				$fileContent = file_put_contents($fullSchemPath.'_review.txt', $review);
			}
			if ($type == 'review') {
				return $review;
			}
			return $schemFiles;
		}
		
		LogBook::setEvent('Warning', __FILE__, __FUNCTION__, "Request for non-existent parameters: {$type}.");
		return array();
	}
	public function getPageDetail()
	{
		return $this->data;
	}
	public function getSchema()
	{
		return $this->currentSchema;
	}
	public function loadPage()
	{
		if ( ! empty($this->currentSchema)) {
			LogBook::setEvent('Warning', __FILE__, __FUNCTION__, 'Attempt to reload page.');
			return;
		}
		global $CORE;
		$this->loadSchema();
		$templatePath= $CORE->getPath2('templates','-dr').$this->data['template'].'/';//for including templates only
		$tempPath    = $_SERVER['DOCUMENT_ROOT'].$templatePath;
		$pagesPath   = $CORE->getPath2('pages');
		$schemesPath = $CORE->getPath2('schemes');
		$title = $this->data['title'];
		$loadSequence = array();
		if ( file_exists($tempPath.'header.php') ) {
			$loadSequence[] = $tempPath.'header.php';
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, "Header of {$this->data['template']} template not exists.");
		}
		
		if ( file_exists($pagesPath.$this->data['page'].'.php') ) {
			$loadSequence[] = $pagesPath.$this->data['page'].'.php';
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'The requested page does not exist.');
			header("HTTP/1.1 404 Not Found");
			$title = '404 ERROR';
			if (file_exists($tempPath.'404.php')) {
				$loadSequence[] = $tempPath.'404.php';
			} elseif (file_exists($pagesPath.'404.php')) {
				$loadSequence[] = $pagesPath.'404.php';
			} else {
				LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'The 404 page does not exist.');
			}
		}

		if ( file_exists($tempPath.'footer.php') ) {
			$loadSequence[] = $tempPath.'footer.php';
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, "Footer of {$this->data['template']} template not exists.");
		}
		foreach ($loadSequence as $key => $value) {
			require_once $value;
		}
	}
	public function loadSchema()
	{
		global $CORE;
		$uriLinks              = array();
		$settings              = $CORE->getProperties1('schema');
		$schemesPath           = $CORE->getPath2('schemes');
		$this->data['fullPath']= explode('?', $_SERVER['REQUEST_URI'])[0];
		foreach ( explode('/', $this->data['fullPath']) as $key => $value) {
			if ($value != '') {
				$uriLinks[] = $value;
			}
		}
		
		if (file_exists($schemesPath.$uriLinks[0].'.txt') ) {
			//если запрашиваемая схема существует
			$currentSchemaName = $uriLinks[0];
			$newUriLinks = array();
			foreach ($uriLinks as $key => $value) {
				//извлечение параметров запроса
				if ($key) {
					$newUriLinks[] = $value;
				}
			}
			$uriLinks = $newUriLinks;
			unset($newUriLinks);
		} else {
			LogBook::setEvent(
				'Warning', __FILE__, __FUNCTION__, 
				"Schema({$uriLinks[0]}) not defined. Attempt to connect the default schema."
			);//предупреждение о попытке подключении схемы по умолчанию
			if ( array_key_exists('default', $settings) && 
				 file_exists($schemesPath.$settings['default'].'.txt')
				){
				$currentSchemaName = $settings['default'];
			} else {
				LogBook::setEvent(
					'Error', __FILE__, __FUNCTION__, 
					'Default schema not defined or not exists.'
				);
				//генерация ошибки подключения схемы
				return;
			}
		}
		$this->data['uriLinks']= $uriLinks;
		$this->data['schema']  = $currentSchemaName;
		$schContent = array();
		if ( $tempContent = file_get_contents($schemesPath.$currentSchemaName.'.txt')) {
			$schContent = $tempContent;
		}
		if ( $schContent = unserialize($schContent)) {
			//if ( ! empty($uriLinks) && ! array_key_exists($uriLinks[0], $schContent)) {
			//	LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'The requested schema does not exist');
			//	return;
			//}
			$this->currentSchema    = $schContent;
			$this->data['template'] = $schContent['_theme'];

			foreach ($uriLinks as $key => $value) {
				//перебо массива параметров
				if (array_key_exists($value, $schContent)) {
					//если параметр является разделом
					if ($schContent['_theme'] != '') {
						//запись текущего шаблона дизайна
						$this->data['template'] = $schContent['_theme'];
					}
					$this->data['page'] = $value;
					$this->data['path'].= '/'.$value;
					$prewContent = $schContent;
					$schContent = $schContent[$value];
					//спуск в раздел схемы
					$this->data['title'] = $schContent['_title'];
					//определение параметров отображения страницы
				} else {
					$this->data['params'][] = $value; 
				}
			}
			if ( empty ($this->data['page']) ){
				$this->data['page'] = $currentSchemaName;
				$this->data['title']= $schContent['_title'];
			}
			$this->data['path'] = '/'.$this->data['schema'].$this->data['path'];
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, "Error reading the $currentSchemaName schema");
		}

	}
	function __construct()
	{
		$this->data     = array(
			'params'  => array(),
			'template'=> '',
			'page'    => '',
			'title'   => '',
			'schema'  => '',
			'uriLinks'=> '',
			'path'    => ''
		);

		$settingsScheme = array(
			'_title'   => 'Настройки',
			'_filename'=> 'settings',
			'_theme'   => 'default',
			'configurations'=> array(
				'_title'   => 'Конфигурации',
				'_theme'   => '',
				'test1' => array(
					'_title'   => 'Тест1',
					'_theme'   => '',
					'test11' => array(
						'_title'   => 'Тест11',
						'_theme'   => '',
					),
					'test12' => array(
						'_title'   => 'Тест12',
						'_theme'   => '',
					),
				),
			),
			'tables' => array(
				'_title'   => 'Таблицы',
				'_theme'   => '',
			),
			'schemes'=> array(
				'_title'   => 'Схемы',
				'_theme'   => '',
				'test2' => array(
					'_title'   => 'Тест2',
					'_theme'   => '',
				),
			),
		);
		global $CORE;
		$settingsPath    = $CORE->getPath2('schemes');
		$settingsFilePath= $settingsPath.'settings.txt';
		$newSch          = $settingsPath.'new.txt';

		if ( ! file_exists($settingsFilePath) ) {
			$content = serialize($settingsScheme);
			file_put_contents($settingsFilePath, $content);
		}

		if ( ! file_exists($newSch) ){
			$content = serialize(array(
				'_title'   => 'Новая',
				'_filename'=> 'New',
				'_theme'   => 'default',
				'page1'=> array(
					'_title'   => 'Новая страница',
					'_theme'   => '',
				)
			));
			file_put_contents($newSch, $content);
		}

		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been connected.');
	}

	function __destruct()
	{
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been disabled.');
	}
}
?>