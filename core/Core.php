<?php

$CORE = NULL; //instantinition of Core
class LogBook
{
//private:
	private static $logs;
//public:
	public static function setEvent(
		$type, 
		$fileLocation, 
		$funcLocation, 
		$discription = NULL
	) {
		static::$logs[$type][] = array(
			$fileLocation, 
			$funcLocation, 
			$discription
		);
	}
	public static function showLog($type = '')
	{
		echo '<pre>';
		if (array_key_exists($type, static::$logs)) {
			var_dump(static::$logs[$type]);
		} else {
			$logTypes = '';
			foreach (static::$logs as $key => $value) {
				$logTypes.= $key.count($value).'/';
			}
			var_dump(array(
				'general' => $logTypes,
				'detail' => static::$logs
				)
			);
		}
		echo '</pre>';
	}
}

class Core
{
//private:
	private static $uniqueInstance = NULL;
	private $modules = array();

	private function __clone()
	{}
	private function __wakeup()
	{}
	private function __sleep()
	{}
	private function __construct()
	{
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'CORE has been initialized.');
	}
	private function connectModule1($className)
	{
		$this->modules[$className] = new $className();
	}
//public:
	public static function getInstance()
	{
		global $CORE;

		if ( ! is_null(static::$uniqueInstance) ) {//проверка на повторное создание объекта
			LogBook::setEvent('Warning', __FILE__, __FUNCTION__, 'Attempt to create another object of CORE.');
			return;//генерация ошибки повторного создания объекта и выход из программы
		}

		static::$uniqueInstance = new Core();
		$CORE = static::$uniqueInstance;//получение объекта ядра и сохранение его ссылки

		$coreDir = opendir(__DIR__);//получение дескриптора каталога
		
		if ($coreDir) {//если получен доступ к папке с библиотеками
			$moduls = array();
			while( false !== ($dirElem = readdir($coreDir)) )
			{
				if ( false !== strpos($dirElem,'.php') && basename(__FILE__) != $dirElem) {
					$moduls[] = $dirElem;
				}//проверку проходят только файлы расширения .php
			}
			closedir($coreDir);//закрытие дескриптора

			foreach ($moduls as $path)
			{
				require_once(__DIR__.'\\'.$path);//подключение библиотеки
				if (isset($instantiationBan)) {//проверка на запрет инстанцирования
					unset($instantiationBan);
					continue;
				}
				$className = explode('.', $path)[0];//получение имени файла
				if (class_exists($className)) {//если существует одноименный файлу класс
					$CORE->connectModule1($className);//создание объекта класса (подключение модуля)
				}
			}
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'No access to the core directory. Connection of modules is impossible.');//генерация ошибки доступа к каталогу
		}
	}

	public static function deleteInstance()
	{
		global $CORE;
		$CORE = NULL;

		if ( ! is_null(static::$uniqueInstance) ) {
			$instance = static::$uniqueInstance;
			static::$uniqueInstance = NULL;
			unset($instance);
		}
	}

	function __destruct()
	{
		foreach($this->modules as $objectName => &$object)
		{
			$temp = $object;
			$object = NULL;
			unset($temp);
		}
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'CORE has been disabled.');
	}

	function __call($name, $arguments)
	{
		$realArgCount   = substr($name, strlen($name) - 1, 1);
		$argPassedCount = count($arguments);
		if ( $realArgCount < $argPassedCount ) {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Mismatch of function arguments - ' . $name);
			return false;
		}
		
		$owner = NULL;
		foreach ($this->modules as $className => &$object) {
			$methods = get_class_methods($className);
			if ( array_search($name, $methods) !== false ) {
				$owner = $object;
				break;
			}
		}
		if ( ! $owner) {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Call to undefined function: ' . $name);
			return false;
		}

		return call_user_func_array(array($owner, $name), $arguments);
	}
	public function modulSet()
	{
		echo '<p>Состав модулей ядра:</p><pre>';
		var_dump(array_keys($this->modules));
		echo '</pre>';
	}

}
