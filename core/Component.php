<?php
class Component 
{
	private $globalData;
	private $componentName;
	public function includeComponent4($class, $name, $template, $PARAMETERS)
	{
		global $CORE;
		$DATA = array();
		$relativeComponentPath= $CORE->getPath2('components','-dr')."{$class}/{$name}/";
		$templatePath = $relativeComponentPath.$template.'/';
		$componentPath= $_SERVER['DOCUMENT_ROOT'].$relativeComponentPath;
		if (file_exists($componentPath.'component.php')) {
			$this->componentName = $name;
			require $componentPath.'component.php';
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, "The specified component ({$class}::{$name}) does not exist.");
		}
		if (empty($DATA)) {
			LogBook::setEvent('Warning', __FILE__, __FUNCTION__, "Template ({$template}) of component ({$class}::{$name}) received no data.");
			return;
		}
		if ( ! file_exists($componentPath.$template.'/')) {
			LogBook::setEvent('Warning', __FILE__, __FUNCTION__, "The specified component template ({$class}::{$name}::{$template}) does not exist. Attempt to connect default template.");
			$template = 'default';
		}
		if (file_exists($componentPath.$template.'/template.php')) {
			require $componentPath.$template.'/template.php';
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Tamplate connect error.');
		}
	}
	public function getComponentName()
	{
		return $this->componentName;
	}
	public function setGlobalData1($DATA)
	{
		$this->globalData[$this->componentName] = $DATA;
	}
	public function getGlobalData1($componentName = NULL)
	{
		if ( ! empty($componentName) ){
			if ( array_key_exists($componentName, $this->globalData) ){
				return $this->globalData[$componentName];
			} else {
				return array();
			}
		} else {
			return $this->globalData;
		}
	}
	function __construct()
	{
		$this->globalData = array();
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been connected.');
	}

	function __destruct()
	{
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been disabled.');
	}
}
?>