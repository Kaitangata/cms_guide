<?php
	$instantiationBan = true;
	function str_split_unicode($str, $l = 0) {
	    if ($l > 0) {
	        $ret = array();
	        $len = mb_strlen($str, "UTF-8");
	        for ($i = 0; $i < $len; $i += $l) {
	            $ret[] = mb_substr($str, $i, $l, "UTF-8");
	        }
	        return $ret;
	    }
	    return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
	}
	function translateTableFields($table = 'general')
	{
		$transTable = 'dictionary';
		global $CORE;
		$words  = array();
		$tables = $CORE->getTable3();
		if ( is_array($tables) && in_array($transTable, $tables) ){
			$tableContent = $CORE->getTable3($transTable, array(
				'where' => array(
					'table' => $table
				)
			))['content'];
			if ( is_array($tableContent) ){
				foreach ($tableContent as $key => $row) {
					$words[$row['word']] = $row['translate'];
				}	
			}
		}
		return $words;
	}
	function isNumericTypeSQL($extType)
	{
		$numeric = array(
			'INT',
			'BIT',
			'FLOAT',
			'DOUBLE',
			'DECIMAL'
		);
		foreach ($numeric as $type) {
			if ( false !== stripos($extType, $type) ){
				return true;
			}
		}
		return false;
	}
	function getIndivPlan($prep_id, $year, $download)
	{
		global $CORE;
		$ynext  = $year+1;
		$ystring= $year.'/'.substr($ynext, 2); 
		//$semester= 1;
		$CORE->query1('SELECT * FROM `users` WHERE `id`='.$prep_id.';');
		$user = $CORE->getQueryResult();
		if ( empty($user) ){
			return 'Такого пользователя не существует.';
		}
		$user = $user[0];
		$currentTable = 'basework';
		$CORE->query1('SELECT * FROM `basework` WHERE `prep_id`='.$prep_id.' AND `year`='.$year.';');
		$plan = $CORE->getQueryResult();
		if ( ! isset($semester) ){
			if ( count($plan) != 2 ){
				return 'База данных содержит недостаточно данных на этот год.';
			}
			$sem1 = array_shift($plan);
			$sem2 = array_shift($plan);
			$dictionary = $CORE->getTable3('dictionary')['content'];
			if ( ! empty($dictionary) ){
				$newDictionary = array();
				foreach ($dictionary as $key => $row) {
					$newDictionary[$row['table']][$row['word']] = $row['translate'];
				}
				$dictionary = $newDictionary;
				unset($newDictionary);
			}
			unset($plan);
			$plan1       = array();
			$ignoreFields= array(
				'prep_id', 'id', 'semester','year'
			);
			$pos = 1;
			foreach ($sem1 as $field => $value) {
				if ( in_array($field, $ignoreFields) ){
					continue;
				}
				if ( array_key_exists($field, $dictionary[$currentTable]) ){
					$name = $dictionary[$currentTable][$field];
				} else {
					$name = $field;
				}
				$plan1[($pos++).'. '.$name] = array(
					$value,'-',$sem2[$field],'-',$value+$sem2[$field],'-','-'
				);
				if ( ! isset($lastRow) ){
					foreach (current($plan1) as $val) {
						$lastRow[] = $val;
					}
				} else {
					foreach (next($plan1) as $key => $val) {
						if ( is_numeric($val) ){
							$lastRow[$key]+= $val;
						}
					}
				}
			}
			$plan1['Всего: '] = $lastRow;
			$max = max($lastRow);
			$CORE->query1('SELECT * FROM `otherwork` WHERE `prep_id` = '.$prep_id.' AND `year`='.$year.';');
			$otherwork = $CORE->getQueryResult();
			$work_in = '';
			foreach ($otherwork as $key => $row) {
				if ( ! empty($row['work_id']) && false === strpos($work_in, $row['work_id'].',')){
					$work_in.= $row['work_id'].', ';
				}
			}
			reset($otherwork);

			if ( $work_in != ''){
				$work_in = substr($work_in, 0, -2);
				$CORE->query1('SELECT * FROM `timerate` WHERE `id` IN ('.$work_in.');');
				$timerate = $CORE->getQueryResult();
			}
			if ( isset($timerate) && ! empty ($timerate) ){
				$plan2     = array();
				$descipl_in= '';
				foreach ($timerate as $tkey => $trow) {
					if ( $trow['per'] == 'descipline' ){
						foreach ($otherwork as $okey => $orow) {
							if ( ! empty($orow['detail']) ){
								$descipl_in.= $orow['detail'].', ';
							}
						}
					}
				}
				if ( $descipl_in != ''){
					$descipl_in = substr($descipl_in, 0, -2);
					$CORE->query1('SELECT * FROM `descipline` WHERE `id` IN ('.$descipl_in.');');
					$desciplinesOld = $CORE->getQueryResult();
					foreach ($desciplinesOld as $row) {
						$desciplines[$row['id']] = $row['name'];

					}
					unset($desciplinesOld);
				}
				reset($timerate);
				reset($otherwork);
				$num = 1;
				foreach ($timerate as $tkey => $trow) {
					foreach ($otherwork as $okey => $orow) {
						if ( $orow['work_id'] == $trow['id'] ){
							$name = $trow['name'];
							if ( $trow['per'] == 'descipline' && ! empty($desciplines) ){
								$name.= $desciplines[$orow['detail']].'.';
							} 
							$plan2[] = array(
								'num'            => $num++,
								'name'           => $name,
								'workload'       => $orow['workload'],
								'hourCurrentYear'=> $orow['workload'],
								'hstart'         => 0,
								'hend'           => 100,
								'tstart'         => '01.09.'.date('y'),
								'tend'           => '30.06.'.(date('y')+1),
							);
						}
					}
				}
			}
		} else {
			echo 'Посеместровый вывод пока не определен.';
		}

		$corePath = $CORE->getPath2('phpWord');
		if ( ! file_exists($wordPath = $corePath.'autoload.php') ){
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Error connecting module PhpWord.');
			return;
		}
		include $wordPath;
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'PhpWord module has been connected.');
		$fileName = 'ИП_'.$year;
		if ( false !== strpos($user['name'], ' ') ){
			$namePath = explode(' ', $user['name']);
			foreach ($namePath as $path) {
				$fileName.= '_'.$path;
			}
		} else {
			$fileName.='_'.$user['name'];
		}
		
		$readerType = 'Word2007';

		$uploadsPath = $CORE->getPath2('uploads');
		$filePath = $uploadsPath.$fileName;
		if ( $readerType == 'Word2007' ){
			$filePath.='.docx';
		} elseif ( $readerType == 'HTML' ){
			$filePath.='.html';
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Reader module doesn\'t exist.');
			return;
		}

		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$section = $phpWord->addSection(array(
			'marginTop'   => 995,
			'marginLeft'  => 1706,
			'marginBottom'=> 1137,
			'marginRight' => 927
		));
		$phpWord->setDefaultFontName('Times New Roman');
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultParagraphStyle(array(
				'spaceBefore'=> 0,
				'spaceAfter' => 0,
				'spacing'    => 0
		));
		$st_boldCaps = array(
			'allCaps'=> true,
			'bold'   => true
		);
		$st_boldUnder14 = array(
			'underline'=>'single',
			'bold'     => true,
			'size'     => 14

		);
		$st_bold = array(
			'bold' => true
		);
		$st_bold11 = $st_bold;
		$st_bold11['size'] = 11;

		$cparag = array(
			'alignment' => 'center'
		);
		$section->addText(
			'индивидуальный план работы преподавателя', 
			array(
				'allCaps'=> true,
				'bold'   => true,
				'size'   => 14
			),
			$cparag
		);

		$textRun = $section->addTextRun($cparag);
		$textRun->addText(
			'на ', 
			$st_boldCaps
		);
		$textRun->addText(
			$ystring, 
			array(
				'underline'=>'single',
				'allCaps'  => true,
				'bold'     => true
			)
		);
		$textRun->addText(
			' учебный год', 
			$st_boldCaps
		);

		$section->addText(
			'(заполняется в двух экземплярах и предоставляется Работодателю в 30-дневный срок со дня заключения трудового договора)', 
			array(
				'name'     => 'Times New Roman',
				'size'     => 10
			),
			$cparag
		);
		$section->addText('', array('size' => '9') );

		$textRun = $section->addTextRun();
		$textRun-> addText('кафедра/центр ', array('bold' => true) );
		$CORE->query1("SELECT `value` FROM `general` WHERE `name`= 'departmentName';");
		$departmentName = $CORE->getQueryResult()[0]['value'];
		$textRun-> addText('«'.$departmentName.'»', $st_boldUnder14 );

		$section->addText('', array('size' => '9') );

		$rank = $user['arank'];
		if (strlen($rank) > 4 ){
			$rank = substr($rank, 0).'.';
		}
		$position = $user['position'];
		if (strlen($position) > 4 ){
			$position = substr($position, 0).'.';
		}
		$userString = $user['adegree'].','.$rank.','.$position.' (0,5) ';
		$section->addText($userString, $st_boldUnder14, $cparag);
		$section->addText('(учёная степень, ученое звание, должность, размер ставки)', NULL, $cparag);

		$section->addText('', array('size' => '9') );

		$section->addText($user['name'], $st_boldUnder14, $cparag);
		$section->addText('(фамилия, имя, отчество)', NULL, $cparag);

		$section->addText('');
		$section->addText('i. учебная работа', $st_boldCaps);
		$cell = array(
			'valign'  => 'top',
		);
		$cellRS_open = array(
			'valign' => 'top',
			'vMerge' => 'restart'
		);
		$cellRS_close = array(
			'vMerge' => 'continue'
		);
		$cellCS = array(
			'valign'  => 'top',
			'gridSpan'=> 2
		);

		$indent = new \PhpOffice\PhpWord\ComplexType\TblWidth(-720);
		$table = $section->addTable( array('borderSize' => 3, 'cellMarginLeft'=>80, 'indent' => $indent,'width' => 10500) );
		$table->addRow();
		$table->addCell(2920, $cellRS_open)->addText('Виды учебных занятий', $st_bold, $cparag);

		$head = array(
			'I семестр', 'II семестр', 'Всего за год'
		);
		foreach ($head as $value) {
			$table->addCell(2000, $cellCS)->addText($value, $st_bold, $cparag);
		}
		unset($head);
		$table->addCell(NULL, $cellRS_open)->addText('Примечание', $st_bold, $cparag);

		$table->addRow();
		$table->addCell(NULL, $cellRS_close);
		for ($i=0; $i < 3; $i++) { 
			$table->addCell(900,  $cell)->addText('план',    $st_bold, $cparag);
			$table->addCell(1100, $cell)->addText('выполн.', $st_bold, $cparag);
		}
		$table->addCell(NULL, $cellRS_close);

		reset($plan1);
		foreach ($plan1 as $key => $row) {
			$table->addRow();
			$table->addCell(NULL, $cell)->addText($key, $st_bold, $bparag );
			foreach ($row as $col => $value) {
				$table->addCell(NULL, $cell)->addText($value, $st_bold11, $cparag);
			}
		}

		$section->addText('В числителе – учебные занятия со студентами, в знаменателе – занятия в рамках программ повышения квалификации и с аспирантами');
		$textRun = $section->addTextRun($cparag);
		$textRun->addText('II. ', array('bold' => true) );
		$textRun->addText('учебно- методическая и воспитательная работа', array('bold' => true, 'allCaps' => true) );
		$section->addText('Работа направлена на совершенствование учебного процесса (разработка учебных планов, рабочих программ, написание учебников, учебных пособий, конспектов лекций, методических руководств и указаний, создание лабораторных работ, разработка тестов, новых технических средств обучения и контроля, проведение олимпиад; участие в воспитательной работе и др.). Конкретный объем часов на виды работ устанавливает заведующий кафедрой в соответствии с приказом ректора университета.', 
			NULL, 
			array(
				'alignment' => 'both',
				'indent'    => -1.18,
				'tabs'       => 360
			) 
		);

		$indent = new \PhpOffice\PhpWord\ComplexType\TblWidth(-720);
		$table = $section->addTable( array('borderSize' => 3, 'cellMarginLeft'=>80, 'indent' => $indent,'width' => 10500) );
		$table->addRow();
		$table->addCell(569,  $cellRS_open)->addText('№ п/п',      $st_bold, $cparag);
		$table->addCell(NULL, $cellRS_open)->addText('Виды работ', $st_bold, $cparag);
		$table->addCell(1263, $cellRS_open)->addText('Общий объём работы (печ. листы, часы)', $st_bold, $cparag);
		$table->addCell(1263, $cellRS_open)->addText('Кол-во часов на текущий уч. год',       $st_bold, $cparag);
		$table->addCell(NULL, $cellCS     )->addText('В % к общему объёму',                   $st_bold, $cparag);
		$table->addCell(NULL, $cellCS     )->addText('Срок исполнения',                       $st_bold, $cparag);

		$table->addRow();
		$table->addCell(NULL,  $cellRS_close);
		$table->addCell(NULL,  $cellRS_close);
		$table->addCell(NULL,  $cellRS_close);
		$table->addCell(NULL,  $cellRS_close);
		$table->addCell(1263,  $cell)->addText('Имеется к началу уч. года',$st_bold, $cparag);
		$table->addCell(1263,  $cell)->addText('Будет к концу уч. года',   $st_bold, $cparag);
		$table->addCell(1263,  $cell)->addText('начало',   $st_bold, $cparag);
		$table->addCell(1263,  $cell)->addText('конец',    $st_bold, $cparag);

		$lastRow = 0;
		if ( ! empty($plan2) ){
			foreach ($plan2 as $key => $row) {
				$table->addRow();
				foreach ($row as $field => $value) {
					if ($field == 'workload'){
						$lastRow+= $value;
					}
					$table->addCell(NULL,  $cell)->addText($value);
				}
			}
		}
		reset($plan2);
		$max +=$lastRow;
		$table->addRow();
		$table->addCell(NULL,  $cell);
		$table->addCell(NULL,  $cell)->addText('Итого: ',$st_bold);
		$table->addCell(NULL,  $cell)->addText($lastRow, $st_bold, $cparag);
		for ($i=count(current($plan2)) - 3; $i > 0 ; $i--) { 
			$table->addCell(NULL,  $cell);
		}
		//$dictionary
		/*
		$st_boldCaps = array(
			'allCaps'=> true,
			'bold'   => true
		);
		$st_boldUnder14 = array(
			'underline'=>'single',
			'bold'     => true,
			'size'     => 14

		);
		$st_bold = array(
			'name' => 'Times New Roman',
			'size' => 12,
			'bold' => true
		);
		$st_bold11 = $st_bold;
		$st_bold11['size'] = 11;

		$cparag = array(
			'alignment' => 'center'
		);
		$cell = array(
			'valign'  => 'top',
		);
		$cellRS_open = array(
			'valign' => 'top',
			'vMerge' => 'restart'
		);
		$cellRS_close = array(
			'vMerge' => 'continue'
		);
		$cellCS = array(
			'valign'  => 'top',
			'gridSpan'=> 2
		);
		*/
		$section->addText('iii. научно-исследовательская работа', $st_boldCaps, $cparag);
		$section->addText('а. Исследовательская', $st_bold, $cparag);

		$tablesName = array(
			'scienwork_a',
			'scienwork_b',
			'scienwork_c',
			'organwork'

		);
		$tables    = array();
		$allTables = $CORE->getTable3();
		foreach ($tablesName as $name) {
			if ( in_array($name, $allTables) ){
				$qres = $CORE->query1("SELECT * FROM `$name` WHERE `prep_id`=$prep_id AND `year`=$year;");
				foreach ($qres as $key => $row) {
					/*foreach ($checkField as $fieldName) {
						if ( array_key_exists($fieldName, $row) && empty($row[$fieldName]) ){
							$row = array();
							break;
						}
					}
					if ( ! empty($row) ){*/
						unset($newRow);
						$newRow['num'] = NULL;
						foreach ($row as $field => $value) {
							if ( ! in_array($field, $ignoreFields) ){
								$newRow[$field] = $value;
							}
						}
						$tables[$name][] = $newRow;
					//}
				}
			}
		}
		$size = array(
			'start' => 2,
			'end'   => 2,
			'name'  => NULL,
			'volume'=> 2.3,
			'num'   => 1,
			'result'=> 2.8,
			'_general'=>2,
		);
		$checkField= array(
			'name'
		);
		$currentTable = 'scienwork_a';
		$lastRow = 0;
		if ( ! empty($tables[$currentTable]) ){
			$table    = $section->addTable( array('borderSize' => 3, 'cellMarginLeft'=>80, 'indent' => $indent,'width' => 10500) );
			$firstRow = current($tables[$currentTable]);
			$count    = count($firstRow);
			$pos      = 1;
			$posCS    = $count-1;
			$style    = $cellRS_open;
			for ($i=0; $i < 2; $i++) {
				$table->addRow();
				foreach ($firstRow as $field => $value) {
					if ( $pos == $posCS && ! $i ){
						$table->addCell(NULL,$cellCS)->addText('В % к общему объёму',$st_bold,$cparag);
						$style = $cellRS_close;
						$pos = 1;
						break;
					}
					if ( array_key_exists($field, $dictionary[$currentTable]) ){
						$name = $dictionary[$currentTable][$field];
					} else {
						$name = $field;
					}
					if ( array_key_exists($field, $size) ){
						$curSize = $size[$field];
					} else {
						$curSize = $size['_general'];
					}
					if ( $pos == $posCS ){
						$style = $cell;
					}
					$table->addCell(santiToTwip($curSize), $style)->addText($name,$st_bold,$cparag);
					$pos++;
				}
			}
			$haveRow = false;
			$num = 0;
			foreach ($tables[$currentTable] as $key => $row) {
				$empty = true;
				foreach ($row as $field => $value) {
					if ( (array_key_exists($field, $checkField) && empty($value)) || 
						($field != 'num' && ! empty($value)) ){
						$empty = false;
						break;
					}
				}
				if ( $empty ){
					continue;
				}
				$table->addRow();
				$row['num']++;
				foreach ($row as $field => $value) {
					$table->addCell(NULL, $cell)->addText($value, NULL, $cparag);
					if ($field == 'volume' && is_numeric($value) ){
						$lastRow+= $value;
					}
				}
				$haveRow = true;
			}
			$table->addRow();
			if ( $haveRow ){
				for ($i=1; $i <= $count; $i++) {
					if ($i == 3){
						$table->addCell(NULL, $cell)->addText($lastRow, $st_bold, $cparag);
					} elseif ($i == 2){
						$table->addCell(NULL, $cell)->addText('Итого: ', $st_bold, $cparag);
					} else {
						$table->addCell(NULL, $cell);
					}
				}
			} else {
				for ($i=0; $i < $count; $i++) { 
					$table->addCell(NULL, $cell);
				}
			}
			
		}
		$max += $lastRow;
		unset($tables[$currentTable]);
		$titles = array(
			'scienwork_b' => array('б. Руководство студенческой научной работой' => $st_bold),
			'scienwork_c' => array('в. Другие виды НИР (подготовка отзывов, докладов, рецензирование и редактирование работ, помощь промышленности, участие в семинарах и конференциях, работа в НТК, организация НИР и др.) __________ часов в год' => $st_bold),
			'organwork'   => array(
				'iv. организационная работа' => $st_boldCaps,
				'(работа в методических комиссиях, организационная работа по кафедре, работы по поручению ректората, деканата, воспитательная работа и др.) 500 часов в год.' => $st_bold
			)
		);
		foreach ($tables as $tableName => $table) {
			if ( array_key_exists($tableName, $titles) ){
				$title = $titles[$tableName];
				$section->addText('');
				foreach ($title as $text => $style) {
					$section->addText($text, $style, $cparag);
				}
				$section->addText('');
			}
			$lastRow = 0;
			$Table = $section->addTable( array('borderSize' => 3, 'cellMarginLeft'=>80, 'indent' => $indent,'width' => 10500) );
			$head  = current($table);
			$count = count($head);
			$Table->addRow();
			foreach ($head as $field => $value) {
				if ( array_key_exists($field, $size) ){
					$cellSize = $size[$field];
				} else {
					$cellSize = $size['_general'];
				}
				if ( array_key_exists($field, $dictionary[$tableName]) ){
					$headFieldTitle = $dictionary[$tableName][$field];
				} else {
					$headFieldTitle = $field;
				}
				$Table->addCell(santiToTwip($cellSize),$cell)->addText($headFieldTitle, $st_bold, $cparag);
			}
			$emptyTable = true;
			$num = 0;
			foreach ($table as $row) {
				$empty = true;
				foreach ($row as $field => $value) {
					if ( (array_key_exists($field, $checkField) && empty($value)) || 
						($field != 'num' && ! empty($value)) ){
						$empty = false;
						break;
					}
				}
				if ( $empty ){
					continue;
				}
				reset($row);
				$Table->addRow();
				$row['num']++;
				foreach ($row as $field => $value) {
					$Table->addCell(NULL, $cell)->addText($value, NULL, $cparag);
					if ( $field == 'volume' && is_numeric($value) ){
						$lastRow+= $value;
					}
				}
				$emptyTable = false;
			}
			$Table->addRow();
			if ( $emptyTable ){
				foreach ($row as $field => $value) {
					$Table->addCell(NULL, $cell)->addText(' ');
				}
			} else {
				for ($i=1; $i <= $count; $i++) {
					if ($i == 3){
						$Table->addCell(NULL, $cell)->addText($lastRow, $st_bold, $cparag);
					} elseif ($i == 2){
						$Table->addCell(NULL, $cell)->addText('Итого: ', $st_bold, $cparag);
					} else {
						$Table->addCell(NULL, $cell);
					}
				}
			}
			$max+=$lastRow;
		}
		$section->addText('');
		$section->addText("ВСЕГО ЧАСОВ: $max", $st_bold);
		$section->addText('');
		$section->addText('');
		$section->addText('Отчёт о выполнение плана будет заслушан на заседании кафедры/центра за I семестр до 1 февраля 2020 г., за II семестр до 30 июня 2020 г.');
		$section->addText('Визы:');
		$section->addText('Заведующий кафедрой/руководитель центра		___________/А.В. Кузнецов/');
		$section->addText('Директор института/декан факультета			___________/Е.В. Сафонов/');
		$section->addText('НачальникУМУ						___________/______________/');
		$section->addText('');
		$section->addText('Работодатель_____________/____________/	Работник ____________/_____________/',$st_bold);
		$section->addText('«___» _____________ 20__ г. 			«___» _____________ 20 __ г.  ',$st_bold);
		$section->addText('');
		$section->addText('Заключение кафедры о выполнении индивидуального годового плана', $st_bold, $cparag);
		for ($i=0; $i < 5; $i++) { 
			$section->addText('__________________________________________________________________');
		}
		$section->addText('');
		$section->addText('«___» _____________ 20 ___ г.     Заведующий кафедрой ___________/А.В. Кузнецов/', NULL, $cparag);
		$section->addText('«___» _____________ 20____г.     Начальник УМУ	     ___________/_______________/ ', NULL, $cparag);
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, $readerType);
		$objWriter->save($filePath);
		if ( $download ){
			file_put_contents($uploadsPath.'download.txt', $filePath);
		}
		return 'Отчет выгружен';
	}
	function santiToTwip($santi)
	{
		if ( is_numeric($santi) ){
			$ratio = 569.152;
			return $santi*$ratio;
		}
		return $santi;
	}
	function getSchChains2($schema, &$result)
	{
		foreach ($schema as $key => $value) {
			if ( ! $key{0} == '_' ){
				continue;
			}
			$result[] = $key;
			getSchChains2($value, $result);
		}
	}
	function map3($array, $col, &$outArray, $ignore = array('_'))
	{
		if ( empty($array)) {
			return;
		}
		$ret = $col;
		foreach ($array as $key => $value) {
			if ( ! in_array($key{0}, $ignore) ){
				$newVal = array();
				foreach ($value as $k => $v) {
					if ( in_array($k{0}, $ignore) ){
						$newVal[$k] = $v;
					}
				}
				$newVal['_filename'] = $key;
				$outArray[] = array(
					'depth'=> $col,
					'cont' => $newVal,
				);

				$someCol = map3($value,$col+1,$outArray,$ignore);
				if ($someCol > $col) {
					$ret = $someCol;
				}
			}
		}
		return $ret;
	}
	function makeMap2($array, $ignore = array('_'))
	{
		$rawMap  = array();
		$maxDepth= map3($array,0,$rawMap,$ignore) + 1;
		$empty   = $rawMap[0]; //template structure of element
		$map     = array();

		foreach ($empty['cont'] as $key => $val) {
			$empty['cont'][$key] = '';
		}
		
		foreach ($rawMap as $key => $value) {
			$map[] = $value;
			$nextKey = $key + 1;
			if (array_key_exists($nextKey, $rawMap)) {
				if ($rawMap[$nextKey]['depth'] == $value['depth']) {
					$empty['depth'] = $value['depth'] + 1;
					$map[] = $empty;
				}
				$diff = $value['depth'] - $rawMap[$nextKey]['depth'];
				if ($diff > 0) {
					$empty['depth'] = $value['depth'] + 1;
					$map[] = $empty;
					for ($i=0; $i < $diff; $i++) { 
						--$empty['depth'];
						$map[] = $empty;
					}
				}
			}
		}
		$last = end($map)['depth'];
		$empty['depth'] = $last + 1;
		$map[] = $empty;
		for (; $last > -1 ; $last--) {
			$empty['depth'] = $last;
			$map[] = $empty;
		}
		$i = 0;
		foreach ($map as $key => $value) {
			if ($value['cont']['_filename'] == '') {
				$map[$key]['cont']['_filename'] = 'new'.$i++;
			}
		}
		return array(
			'arr'     => $map,
			'maxDepth'=> $maxDepth
		);
	}
	function demap($array, &$result)
	{
		$deleteDepth = -1;
		$tempArr = array();
		foreach ($array as $key => $value) {
			$pageDetail = explode('_', $key);
			$tempArr[$pageDetail[0]]['_'.$pageDetail[1]] = $value;
		}
		$tempArr2 = array();
		$i = -1;
		foreach ($tempArr as $key => $value) {
			if ( $deleteDepth == -1 && array_key_exists('_del', $value)) {
				$deleteDepth = $value['_depth'];
				continue;
			}
			
			if ( $deleteDepth > -1 ) {
				if ($value['_depth'] == $deleteDepth && ! array_key_exists('_del', $value)) {
					$deleteDepth = -1;
				} else {
					continue;
				}
			}
			if ( false !== strpos($value['_filename'], 'new')) {
				continue;
			}
			$tempArr2[] = $value;
		}
		unset($tempArr);
		$attemps = count($tempArr2);

		while ($attemps) {
			foreach ($tempArr2 as $key => $value) {
				if (empty($value)) {
					continue;
				}
				if (array_key_exists($key + 1 , $tempArr2) && ! empty($tempArr2[$key + 1])) {
					$nextDepth = $tempArr2[$key + 1]['_depth'];
					if ($nextDepth > $value['_depth']) {
						continue;
					}
				}
				for ($i=$key; $i >= 0; $i--) {
					if ( empty($tempArr2[$i]) ){
						continue;
					} 
					if ($tempArr2[$i]['_depth'] < $value['_depth']) {
						$filename = $value['_filename'];
						unset($value['_depth']);
						unset($value['_filename']);
						$tempArr2[$i][$filename] = $value;
						$tempArr2[$key] = array();
						break;
					} else {
						continue;
					}
				}
			}
			$attemps--;
		}
		foreach ($tempArr2 as $key => $value) {
			if( empty($value) ) {
				continue;
			}
			$filename = $value['_filename'];
			unset($value['_filename']);
			unset($value['_depth']);
			$result[$filename] = $value;
		}
		unset($tempArr2);
	}
	function varDump($data)
	{
		echo '<pre>';
		var_dump($data);
		echo '</pre>';
	}
?>