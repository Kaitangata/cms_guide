<?php
class Data
{
	private $connectionInstance;
	private $queryResult;
	private $connectionProperties;
	private $safeMode;
	private function openConnection()
	{
		global $CORE;
		if ( $this->connectionInstance ){
			LogBook::setEvent('Warning', __FILE__, __FUNCTION__, 'Attempt to reconnect to the database.');
			return true;
		}
		if ( ! $connectionProp = $CORE->getProperties1('connection') ){
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Error transferring connection parameters.');
			return false;
		}
		$this->connectionInstance = new mysqli(
			$connectionProp['host'], 
			$connectionProp['username'], 
			$connectionProp['password'], 
			$connectionProp['dbname'],
			$connectionProp['port']
		);

		if ( mysqli_connect_error() ) {
			LogBook::setEvent(
				'Error', __FILE__, __FUNCTION__, 
				'Connection error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error()
			);
			unset($this->connectionInstance);
			$this->connectionInstance = NULL;
			return false;
		}
		$this->connectionProperties = $connectionProp;
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Database connection successfully established.');
		return true;
	}
	private function closeConnection()
	{
		if ( ! $this->connectionInstance ) {
			LogBook::eMess('DataBase', 'The connection was not established...');
			return;
		}
		if ( ! $this->connectionInstance->close() ) {
			LogBook::eMess('DataBase', 'Closing connection failed with error.');
			return;
		}
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Connection succesfully closed.');
		unset($this->connectionInstance);
		$this->connectionInstance = NULL;
	}
	private function parseQuery($query)
	{
		$patterns     = '/{dbname}/';
		$replacements = $this->connectionProperties['dbname'];
		$result = preg_replace($patterns, $replacements, $query);
		return $result;
	}
	public function changeSafeMode()
	{
		$this->safeMode = ! $this->safeMode;
	}
	public function query1($query)
	{
		$this->queryResult= array();
		if ( ! $this->openConnection() ){
			return false;
		}
		$query            = $this->parseQuery($query);
		$mysqliObject     = $this->connectionInstance->query($query);
		$result = false;
		if ( is_object($mysqliObject) ) {
			while ( $row = $mysqliObject->fetch_assoc() ) {
	    		$this->queryResult[] = $row;
			}
			$mysqliObject->free();
			$result = $this->queryResult;
		} elseif ( $mysqliObject ) {
			$result = true;
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Invalid request('.$query.') to the database.');
		}
		unset($mysqliObject);
		$this->closeConnection();
		return $result;
	}
	public function getQueryResult()
	{
		return $this->queryResult;
	}
	public function updateTable3($tableName, $content, $addition = array())
	{
		global $CORE;
		$describe  = $CORE->getTableDesc1($tableName);
		$query   = array();
		$primaryKey= '';
		$result    = array(
			'content'   => array(),
			'fields'    => $describe,
			'error'     => array(),
			'detail'    => array()
		);
		$empty = array();
		foreach ($describe as $key => $value) {
			if ($value['Key'] == 'PRI') {
				$primaryKey = $key;
				break;
			}
		}
		reset($describe);
		if ( $primaryKey == '' ){
				$primaryKey = key($describe);
		}

		$newAddition = array();
		foreach ($addition as $id => $row) {
			$realPrimaryKeyValue = $row[$primaryKey];
			if ( $realPrimaryKeyValue != $id ){
				$newAddition[$realPrimaryKeyValue] = $row;
			} else {
				$newAddition[$id] = $row;
			}
		}
		$addition = $newAddition;
		unset($newAddition);
		
		foreach ($describe as $field => $params) {
			foreach ($content as $id => $row) {

				if (empty($row) || 
					in_array($id, $result['error'])
				){
					continue;
				}

				foreach ($row as $key => $value) {
					if ( ! array_key_exists($key, $describe) ){
						$result['error'][] = $id;
						continue;
					}
				}
				if ( ! array_key_exists($field, $row) ){
					continue;
				}

				if ( $params['Null'] == 'NO' && 
					( $row[$field] == '' || (false !== strpos($describe[$field]['Type'], 'int') && ! is_numeric($row[$field])))
				){
					$result['error'][] = $id;
					continue;
				}

			}
			foreach ($addition as $id => $row) {
				foreach ($row as $key => $value) {
					if ( ! array_key_exists($key, $describe) ){
						unset($addition[$id]);
						continue;
					}
				}
				if ( empty($row[$field]) ){
					if ( $params['Null'] == 'NO' ){
						unset($addition[$id]);
						continue;
					}
					$empty[$id]++;
				}
				if ( $params['Null'] == 'NO' && 
					( $row[$field] == '' || (false !== strpos($describe[$field]['Type'], 'int') && ! is_numeric($row[$field])))
				){
					unset($addition[$id]);
				}
			}
		}
		$fieldsCount = count($describe);
		if ( ! empty($empty) ){
			foreach ($empty as $id => $count) {
				if ( $count == $fieldsCount ){
					unset($addition[$id]);
				}
			}
		}
		$lastID = '';
		foreach ($content as $id => $row) {
			if ( empty($row) ){
				if ( ! empty($addition) ){
					$transfer = array_shift($addition);
					$transfer[$primaryKey] = $id;
					$content[$id] = $transfer;
				} else {
					if ( $this->safeMode ){
						LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Call ('.__LINE__.') to database has been blocked');
					} else {
						$query[] = "DELETE FROM $tableName WHERE $primaryKey={$id};";
					}
					unset($content[$id]);
					continue;
				}
			}
			if ( ! in_array($id, $result['error']) ){
				$fields = '';
				foreach ($content[$id] as $field => $value) {
					if ( false === strpos($describe[$field]['Type'], 'int') ){
						$value = '\''.$value.'\'';
					} elseif ($value != '') {
						$value = (int)$value;
					} else {
						continue;
					}
					$fields.= $field.'='.$value.', ';
				}
				$fields = substr($fields, 0, -2);
				if ( $this->safeMode ){
					LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Call ('.__LINE__.') to database has been blocked');
				} else {
					$query[] = "UPDATE $tableName SET $fields WHERE $primaryKey={$id};";
				}
				
			}
			$lastID = $id;
		}
		if ( ! empty($addition) ){
			foreach ($addition as $id => $row) {
				$fields = '';
				$values = '';
				if ( false !== strpos($describe[$primaryKey]['Type'], 'int') ){
					$row[$primaryKey] = ++$lastID;
					$content[$lastID] = $row;
				} else {
					$content[$id] = $row;
				}

				foreach ($row as $field => $fvalue) {
					$fields.=$field.', ';
					if ( false === strpos($describe[$field]['Type'], 'int') ){
						$fvalue = '\''.$fvalue.'\'';
					} else {
						$fvalue = (int)$fvalue;
					}
					$values.=$fvalue.', ';
				}
				$fields = substr($fields, 0, -2);
				$values = substr($values, 0, -2);
				if ( $this->safeMode ){
					LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Call ('.__LINE__.') to database has been blocked');
				} else {
					$query[] = "INSERT INTO $tableName ({$fields}) values({$values});";
				}
				$fields = '';
				$values = '';
			}
		}
		$result['content'] = $content;
		$result['detail']['maxID'] = $lastID;
		foreach ($query as $key => $value) {
			$CORE->query1($value);
		}
		return $result;
	}
	public function getTableDesc1($tableName)
	{
		$qres = $this->query1('DESCRIBE '.$tableName.';');
		if ( is_array($qres) ){
			foreach ($qres as $key => $value) {
				$fieldName = $value['Field'];
				unset($value['Field']);
				if ( false !== strpos($value['Type'], 'enum') ){
					foreach (explode('\'', $value['Type']) as $key => $val) {
						if ( $val == ',' || $val == ')' || $val == 'enum(' ){
							continue;
						}
						$value['Values'][] = $val;
					}
					$value['Type'] = 'Enum';
				}
				$result[$fieldName] = $value;
			}
		} else {
			$result = $qres;
		}
		return $result;
	}
	public function getTable3($tableName='', $condition = array(), $desc = false)
	{
		if ( empty($tableName) ){
			$this->query1('SHOW TABLES FROM {dbname};');
			foreach ($this->queryResult as $key => $value) {
				$result[] = current($value);
			}
		} else {
			$describe = $this->getTableDesc1($tableName);
			if ( ! is_array($describe) ){
				return false;
			}
			$fields = '';
			$where  = '';
			if ( is_array($condition) ){
				if ( array_key_exists('fields', $condition) ){
					foreach ($condition['fields'] as $field) {
						if ( ! is_string($value) || ! array_key_exists($field, $describe) ){
							continue;
						}
						$fields.="`$field`, ";
					}
					if ( $fields != '' ){
						$fields = substr($fields, 0, -2);
					}
					foreach ($condition['where'] as $field => $value) {
						if ( ! is_string($value) || $value == '' || ! array_key_exists($field, $describe) ){
							continue;
						}
						if ( isNumericTypeSQL($describe[$field]['Type']) ){
							$where.="`{$field}`=$value AND ";
						} else {
							$where.="`{$field}`='$value' AND ";
						}
					}
					if ( $where != '' ){
						$where = substr($where, 0, -5);
					}
				}
			}
			$primaryKey= '';
			foreach ($describe as $key => $value) {
				if ($value['Key'] == 'PRI') {
					$primaryKey = $key;
					break;
				}
			}
			reset($describe);
			if ( $primaryKey == '' ){
				$primaryKey = key($describe);
			}
			if ( $desc ){
				$result['fields'] = $describe;
			}
			$qres = $this->query1('SELECT * FROM '.$tableName.';');
			$lastID = 0;
			if ( empty($qres) ){
				$fields = '';
				$values = '';
				foreach ($describe as $field => $value) {
					$fields.= $field.', ';
					if ( isNumericTypeSQL($value['Type']) ){
						$fvalue = '1';
						$row[$field] = 1;
					} else {
						varDump($value);
						$fvalue = '\''.'empty'.'\'';
						$row[$field] = 'empty';
					}
					$values.=$fvalue.', ';
				}
				$fields = substr($fields, 0, -2);
				$values = substr($values, 0, -2);
				$lastID = 1;
				$currentID = $row[$primaryKey];
				$result['content'][$currentID] = $row;
				if ( $this->safeMode ){
					LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Call ('.__LINE__.') to database has been blocked');
				} else {
					$this->query1("INSERT INTO $tableName ({$fields}) values({$values});");
				}
				
			} else {
				foreach ($this->queryResult as $key => $value) {
					$currentID = $value[$primaryKey];
					$result['content'][$currentID] = $value;
					$lastID = $currentID;
				}
			}
			
			$result['detail']['maxID'] = $lastID;
		}
		return $result;
	}
	function __construct()
	{
		$this->safeMode            = false;
		$this->queryResult         = NULL;
		$this->connectionInstance  = NULL;
		$this->connectionProperties= NULL;
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been connected.');
	}
	function __destruct()
	{
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been disabled.');
	}
}
?>