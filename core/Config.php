<?php

class Config 
{
	//private:
	private $paths;
	//public:
	public function getPath2($module, $param = '')
	{
		$path = $_SERVER['DOCUMENT_ROOT'];
		if (array_key_exists($module, $this->paths)) {
			if ($param == '-dr') {
				$path = '';
			}
			$path.= $this->paths[$module];
		}
		return $path;
	}
	public function createConfigFile2($name, $content)
	{
		file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->paths['configs'].$name.'.txt', serialize($content));
	}
	public function getProperties1($type)
	{
		$fullConfPath = $this->getPath2('configs');
		if ($type == 'review' && file_exists($fullConfPath.'_review.txt')) {
			$fileContent = file_get_contents($fullConfPath.'_review.txt');
			return unserialize($fileContent);
		}
		$conFiles = array();
		if ($hdir = opendir($fullConfPath)) {
			while(false !== ($conFile = readdir($hdir)) )
			{
				if (false !== strpos($conFile,'.txt') && $conFile{0} != '_') {
					$conFiles[basename($conFile, '.txt')] = array();
				}
			}
			closedir($hdir);
		}
		if (array_key_exists($type, $conFiles)) {
			$fileContent = file_get_contents($fullConfPath.$type.'.txt');
			return unserialize($fileContent);
		}
		if ($type == 'all' || $type == 'review') {
			$review = array();
			foreach ($conFiles as $key => &$value) {
				$fileContent = file_get_contents($fullConfPath.$key.'.txt');
				$value       = unserialize($fileContent);
				$review[$key]= array(
					'_title' => $value['_title']
				);
			}
			if ( ! file_exists($fullConfPath.'_review.txt')) {
				$review = serialize($review);
				$fileContent = file_put_contents($fullConfPath.'_review.txt', $review);
			}
			if ($type == 'review') {
				return $review;
			}
			return $conFiles;
		}
		
		LogBook::setEvent('Warning', __FILE__, __FUNCTION__, "Request for non-existent parameters: {$type}.");
		return array();
	}
	
	function __construct()
	{
		$configFilesParam = array(
			'connection' => array(
				'_title'    => 'Настройки подключения к базе данных',
				'host'		=> '127.0.0.1',
				'username' 	=> 'root',
				'password' 	=> '',
				'dbname'	=> 'generalBase',
				'port'		=> '3306'
			),
			'schema' => array(
				'_title'    => 'Настройки схем',
				'default'   => 'settings'
			),
		);
		$this->paths = array(
			'configs'   =>'/files/configs/',
			'schemes'   =>'/files/schemes/',
			'pages'     =>'/pages/',
			'templates' =>'/files/templates/',
			'components'=>'/files/components/',
			'core'      =>'/core/',
			'uploads'   =>'/files/uploads/',
			'tfile'     =>'/files/uploads/templates/',
			'phpWord'   =>'/core/vendor/'
		);

		$conFiles = array();
		$fullConfPath = $_SERVER['DOCUMENT_ROOT'].$this->paths['configs'];
		if ($hdir = opendir($fullConfPath)) {
			while(false !== ($conFile = readdir($hdir)) )
			{
				if (false !== strpos($conFile,'.txt')) {
					$conFiles[basename($conFile, '.txt')] = '';
				}
			}
			closedir($hdir);
		}
		foreach ($configFilesParam as $key => $value) {
			if ( ! array_key_exists($key, $conFiles)) {
				$content = serialize($value);
				file_put_contents($fullConfPath.$key.'.txt', $content);
				LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Config file ('.$key.') has been created.');
			}
		}
		
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been connected.');
	}

	function __destruct()
	{
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been disabled.');
	}
}