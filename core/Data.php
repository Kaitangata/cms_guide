<?php
class Data
{
	private $connectionInstance;
	private $queryResult;
	private $connectionProperties;
	private $safeMode;
	private function openConnection()
	{
		global $CORE;
		if ( $this->connectionInstance ){
			LogBook::setEvent('Warning', __FILE__, __FUNCTION__, 'Attempt to reconnect to the database.');
			return true;
		}
		if ( ! $connectionProp = $CORE->getProperties1('connection') ){
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Error transferring connection parameters.');
			return false;
		}
		$this->connectionInstance = @new mysqli(
			$connectionProp['host'], 
			$connectionProp['username'], 
			$connectionProp['password'], 
			$connectionProp['dbname'],
			$connectionProp['port']
		);

		if ( mysqli_connect_error() ) {
			LogBook::setEvent(
				'Error', __FILE__, __FUNCTION__, 
				'Connection error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error()
			);
			unset($this->connectionInstance);
			$this->connectionInstance = NULL;
			return false;
		}
		$this->connectionProperties = $connectionProp;
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Database connection successfully established.');
		return true;
	}
	private function closeConnection()
	{
		if ( ! $this->connectionInstance ) {
			LogBook::eMess('DataBase', 'The connection was not established...');
			return;
		}
		if ( ! $this->connectionInstance->close() ) {
			LogBook::eMess('DataBase', 'Closing connection failed with error.');
			return;
		}
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, 'Connection succesfully closed.');
		unset($this->connectionInstance);
		$this->connectionInstance = NULL;
	}
	private function parseQuery($query)
	{
		$patterns     = '/{dbname}/';
		$replacements = $this->connectionProperties['dbname'];
		$result = preg_replace($patterns, $replacements, $query);
		return $result;
	}
	public function checkStructure()
	{
		global $CORE;
		$infoFile = $CORE->getPath2('uploads').'download.txt';
		if ( file_exists($infoFile) ){
			$downloadFilePath = file_get_contents($infoFile);
			if ( file_exists($downloadFilePath) ){
				header('Content-Description: File Transfer');
			    header('Content-Type: application/octet-stream');
			    header('Content-Disposition: attachment; filename=' . basename($downloadFilePath));
			    header('Content-Transfer-Encoding: binary');
			    header('Expires: 0');
			    header('Cache-Control: must-revalidate');
			    header('Pragma: public');
			    header('Content-Length: ' . filesize($downloadFilePath));
			    
			    readfile($downloadFilePath);
			}
			unlink($infoFile);
		}
		$year     = date('Y');
		$users    = $CORE->query1('SELECT `id` FROM `users` WHERE NOT `role`=\'student\';');
		$exiTables= $CORE->getTable3();
		$tables   = array(
			'basework',
			'otherwork',
			'scienwork_a',
			'scienwork_b',
			'scienwork_c',
			'organwork'
		);
		if ( ! is_array($users) || empty($users) || ! is_array($exiTables) || empty($exiTables) ){
			return;
		}
		foreach ($users as $key => $row) {
			$newUsersStruct[] = $row['id'];
		}
		$users = $newUsersStruct;
		foreach ($tables as $tableName) {
			if ( in_array($tableName, $exiTables) ){
				//$desc = $CORE->getTableDesc1($tableName);
				$table = $CORE->query1('SELECT `prep_id`,`semester`,`year` FROM '.$tableName.';');
				if ( is_array($table) ){
					if ( empty($table) ){
						$table[0] = array(
							'prep_id' => '',
							'semester' => '',
							'year' => ''
						);
					}
					$values = '';
					for ($semester=1; $semester < 3; $semester++) { 
						foreach ($users as $prep_id) {
							$insert = true;
							foreach ($table as $key => $row) {
								if ( $row['prep_id'] == $prep_id && $row['semester'] == $semester && $row['year'] == $year ){
									$insert = false;
									break;
								}
							}
							if ( $insert ){
								$values.="('$prep_id', '$semester', '$year'), ";
							}
						}	
					}
					
					if ( $values != '' ){
						$values = substr($values, 0, -2);
						$CORE->query1("INSERT INTO `$tableName`(`prep_id`,`semester`,`year`) VALUES $values;");
					}
				}
			}
		}
	}
	public function changeSafeMode()
	{
		$this->safeMode = ! $this->safeMode;
	}
	public function query1($query)
	{
		if ( $this->safeMode ){
			$banCom = array(
				'DELETE', 'UPDATE', 'INSERT'
			);
			foreach ($banCom as $value) {
				if ( false !== strpos($query, $value) ){
					LogBook::setEvent('Warning', __FILE__, __FUNCTION__, 'Call to database has been blocked ('.$query.').');
					return false;
				}
			}
		}
		$this->queryResult= array();
		if ( ! $this->openConnection() ){
			return false;
		}
		$query            = $this->parseQuery($query);
		$mysqliObject     = $this->connectionInstance->query($query);
		$result = false;
		if ( is_object($mysqliObject) ) {
			while ( $row = $mysqliObject->fetch_assoc() ) {
	    		$this->queryResult[] = $row;
			}
			$mysqliObject->free();
			$result = $this->queryResult;
		} elseif ( $mysqliObject ) {
			$result = true;
		} else {
			LogBook::setEvent('Error', __FILE__, __FUNCTION__, 'Invalid request('.$query.') to the database.');
		}
		unset($mysqliObject);
		$this->closeConnection();
		return $result;
	}
	public function getQueryResult()
	{
		return $this->queryResult;
	}
	public function updateTable3($tableName, $content, $addition = array())
	{
		global $CORE;
		$describe  = $CORE->getTableDesc1($tableName);
		$query     = array();
		$primaryKey= '';
		$primaryKeyType = '';
		$primaryKeyExtra= '';
		$result    = array(
			'content'   => array(),
			'fields'    => $describe,
			'error'     => array(),
			'detail'    => array()
		);
		$empty = array();
		foreach ($describe as $key => $value) {
			if ($value['Key'] == 'PRI') {
				$primaryKey     = $key;
				$primaryKeyType = $value['Type'];
				$primaryKeyExtra= $value['Extra'];
				break;
			}
		}
		reset($describe);
		if ( $primaryKey == '' ){
				$primaryKey = key($describe);
		}
		if ( ! isNumericTypeSQL($primaryKeyType) && array_key_exists($primaryKey, current($addition)) ){
			$newAddition = array();
			foreach ($addition as $id => $row) {
				$realPrimaryKeyValue = $row[$primaryKey];
				if ( $realPrimaryKeyValue != $id ){
					$newAddition[$realPrimaryKeyValue] = $row;
				} else {
					$newAddition[$id] = $row;
				}
			}
			$addition = $newAddition;
			unset($newAddition);	
		}
		
		foreach ($describe as $field => $params) {
			foreach ($content as $id => $row) {

				if (empty($row) || 
					in_array($id, $result['error'])
				){
					continue;
				}

				foreach ($row as $key => $value) {
					if ( ! array_key_exists($key, $describe) ){
						$result['error'][] = $id;
						continue;
					}
				}
				if ( ! array_key_exists($field, $row) ){
					continue;
				}

				if ( $params['Null'] == 'NO' && 
					( $row[$field] == '' || (isNumericTypeSQL($describe[$field]['Type']) && ! is_numeric($row[$field])))
				){
					$result['error'][] = $id;
					continue;
				}

			}
			foreach ($addition as $id => $row) {
				foreach ($row as $key => $value) {
					if ( ! array_key_exists($key, $describe) ){
						unset($addition[$id]);
						continue;
					}
				}
				if ( array_key_exists($field, $row) ){
					if (  empty($row[$field]) ){
						if ( $params['Null'] == 'NO' ){
							unset($addition[$id]);
							
						} else {
							$empty[$id]++;
						}
					} elseif ( isNumericTypeSQL($describe[$field]['Type']) && ! is_numeric($row[$field]) ){
						unset($addition[$id]);
						
					}
				} else {
					if ( $field != $primaryKey && $params['Null'] == 'NO' ){
						unset($addition[$id]);
					}
				}
			}
		}
		reset($addition);
		$fieldsCount = count(current($addition)) - 1;
		if ( ! empty($empty) ){
			foreach ($empty as $id => $count) {
				if ( $count == $fieldsCount ){
					unset($addition[$id]);
				}
			}
		}
		foreach ($content as $id => $row) {
			if ( empty($row) ){
				if ( ! empty($addition) ){
					$transfer = array_shift($addition);
					$transfer[$primaryKey] = $id;
					$content[$id] = $transfer;
				} else {
					$query[] = "DELETE FROM $tableName WHERE `$primaryKey`={$id};";
					unset($content[$id]);
					continue;
				}
			}
			if ( ! in_array($id, $result['error']) ){
				$fields = '';
				foreach ($content[$id] as $field => $value) {
					if ( ! isNumericTypeSQL($describe[$field]['Type']) ){
						$value = '\''.$value.'\'';
					} elseif ($value != '') {
						$value = (int)$value;
					} else {
						continue;
					}
					$fields.= "`$field`=$value, ";
				}
				$fields = substr($fields, 0, -2);
				$query[] = "UPDATE $tableName SET $fields WHERE $primaryKey={$id};";
			}
		}

		if ( ! empty($addition) ){
			if ( isNumericTypeSQL($primaryKeyType) ){
				if ( $primaryKeyExtra != '' ){//auto_inc
					$tableStat = $this->query1("SHOW TABLE STATUS FROM `{dbname}` WHERE `name` LIKE '$tableName';");
					$lastID    = $tableStat[0]['Auto_increment'];
				} else {
					$maxID  = $this->query1("SELECT MAX(`$primaryKey`) FROM `$tableName`")[0];
					$lastID = current($maxID) + 1;
				}
			}
			foreach ($addition as $id => $row) {
				$fields = '';
				$values = '';
				if ( isNumericTypeSQL($primaryKeyType) ){
					$row[$primaryKey] = $lastID;
					$content[$lastID] = $row;
					$lastID++;
				} else {
					$content[$id] = $row;
				}
				foreach ($row as $field => $fvalue) {
					$fields.='`'.$field.'`, ';
					if ( ! isNumericTypeSQL($describe[$field]['Type']) ){
						$fvalue = '\''.$fvalue.'\'';
					} else {
						$fvalue = (int)$fvalue;
					}
					$values.=$fvalue.', ';
				}
				$fields = substr($fields, 0, -2);
				$values = substr($values, 0, -2);
				$query[] = "INSERT INTO $tableName ({$fields}) values({$values});";
				$fields = '';
				$values = '';
			}
		}
		$result['content'] = $content;
		$result['detail']['maxID'] = $lastID;
		foreach ($query as $key => $value) {
			$CORE->query1($value);
		}
		return $result;
	}
	public function getTableDesc1($tableName)
	{
		$qres = $this->query1('DESCRIBE '.$tableName.';');
		if ( is_array($qres) ){
			foreach ($qres as $key => $value) {
				$fieldName = $value['Field'];
				unset($value['Field']);
				if ( false !== strpos($value['Type'], 'enum') ){
					foreach (explode('\'', $value['Type']) as $key => $val) {
						if ( $val == ',' || $val == ')' || $val == 'enum(' ){
							continue;
						}
						$value['Values'][] = $val;
					}
					$value['Type'] = 'Enum';
				}
				$result[$fieldName] = $value;
			}
		} else {
			$result = $qres;
		}
		return $result;
	}
	public function getTable3($tableName='', $condition = array(), $desc = false)
	{
		if ( empty($tableName) ){
			$this->query1('SHOW TABLES FROM `{dbname}`;');
			foreach ($this->queryResult as $key => $value) {
				$result[] = current($value);
			}
		} else {
			$describe = $this->getTableDesc1($tableName);
			if ( ! is_array($describe) ){
				return false;
			}
			$fields = '';
			$where  = '';
			if ( is_array($condition) ){
				if ( array_key_exists('fields', $condition) ){
					foreach ($condition['fields'] as $field) {
						if ( empty($field) || ! array_key_exists($field, $describe) ){
							continue;
						}
						$fields.="`$field`, ";
					}
					if ( $fields != '' ){
						$fields = substr($fields, 0, -2);
					}
				}
				if ( array_key_exists('where', $condition) ){
					foreach ($condition['where'] as $field => $value) {
						if ( empty($value) || ! array_key_exists($field, $describe) ){
							continue;
						}
						if ( isNumericTypeSQL($describe[$field]['Type']) ){
							if ( is_numeric($value) ){
								$where.="`{$field}`=$value AND ";
							}
						} else {
							$where.="`{$field}`='$value' AND ";
						}
					}
					if ( $where != '' ){
						$where = substr($where, 0, -5);
					}
				}
			}
			$primaryKey     ='';
			$primaryKeyType ='';
			$primaryKeyExtra='';
			foreach ($describe as $key => $value) {
				if ($value['Key'] == 'PRI') {
					$primaryKey     = $key;
					$primaryKeyType = $value['Type'];
					$primaryKeyExtra= $value['Extra'];
					break;
				}
			}
			reset($describe);
			if ( $primaryKey == '' ){
				$primaryKey = key($describe);
			}
			if ( $desc ){
				$result['fields'] = $describe;
			}
			$query = 'SELECT ';
			if ( $fields != '' ){
				$query.= $fields;
			} else {
				$query.= '*';
			}
			$query.= ' FROM `'.$tableName.'`';
			if ( $where != '' ){
				$query.= ' WHERE '.$where.';';
			} else {
				$query.= ';';
			}
			$qres = $this->query1($query);
			if ( is_array($qres) ){
				foreach ($this->queryResult as $key => $value) {
					$currentID = $value[$primaryKey];
					$result['content'][$currentID] = $value;
				}
			}
			if ( isNumericTypeSQL($primaryKeyType) ){
				if ( $primaryKeyExtra != '' ){//auto_inc
					$tableStat = $this->query1("SHOW TABLE STATUS FROM `{dbname}` WHERE `name` LIKE '$tableName';");
					$lastID    = $tableStat[0]['Auto_increment'];
				} else {
					$maxID  = $this->query1("SELECT MAX(`$primaryKey`) FROM `$tableName`")[0];
					$lastID = current($maxID) + 1;
				}
			}
			$result['detail']['maxID'] = $lastID;
		}
		return $result;
	}
	function __construct()
	{
		$this->safeMode            = false;
		$this->queryResult         = NULL;
		$this->connectionInstance  = NULL;
		$this->connectionProperties= NULL;
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been connected.');
	}
	function __destruct()
	{
		LogBook::setEvent('Message', __FILE__, __FUNCTION__, __CLASS__.' module has been disabled.');
	}
}
?>