<?
$prep_id = 4;
$CORE->includeComponent4(
	'menu',
	'menuByTableFields',
	'default',
	array(
		'target' => 'basework',
		'fields' => array(
			'year'     => 'Год',
			'semester' => 'Семестр'
		)
	)
);
$CORE->includeComponent4(
	'observer',
	'tableWithoutMenu',
	'default',
	array(
		'data'  => 'menuByTableFields',
		'table' => 'organwork',
		'fhidden'=> array(
			'id', 'prep_id', 'semester', 'year'
		),
		'fields'=> array(
			'prep_id' => $prep_id,
		)
	)
);
?>