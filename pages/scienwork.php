<?
$prep_id = 4;
$CORE->includeComponent4(
	'menu',
	'menuByTableFields',
	'default',
	array(
		'target' => 'basework',
		'fields' => array(
			'year'     => 'Год',
			'semester' => 'Семестр'
		)
	)
);
$CORE->includeComponent4(
	'observer',
	'tableWithoutMenu',
	'default',
	array(
		'data'  => 'menuByTableFields',
		'title' => 'а. Исследовательская',
		'table' => 'scienwork_a',
		'fhidden'=> array(
			'id', 'prep_id', 'semester', 'year'
		),
		'fields'=> array(
			'prep_id' => $prep_id,
		)
	)
);
$CORE->includeComponent4(
	'observer',
	'tableWithoutMenu',
	'default',
	array(
		'data'  => 'menuByTableFields',
		'title' => 'б. Руководство студенческой научной работой',
		'table' => 'scienwork_b',
		'fhidden'=> array(
			'id', 'prep_id', 'semester', 'year'
		),
		'fields'=> array(
			'prep_id' => $prep_id,
		)
	)
);
$CORE->includeComponent4(
	'observer',
	'tableWithoutMenu',
	'default',
	array(
		'data'  => 'menuByTableFields',
		'title' => 'в. Другие виды НИР (подготовка отзывов, докладов, рецензирование и редактирование работ, помощь промышленности, участие в семинарах и конференциях, работа в НТК, организация НИР и др.)',
		'table' => 'scienwork_c',
		'fhidden'=> array(
			'id', 'prep_id', 'semester', 'year'
		),
		'fields'=> array(
			'prep_id' => $prep_id,
		)
	)
);
?>