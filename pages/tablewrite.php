<style>
table.table td {
	border: 2px solid black;
}
</style>
<?
$pageDetail  = $CORE->getPageDetail();
$selectedYear= '';
$selectedSem = '';
$size = 3;
$table       = array();
$currentYear = date('Y');
$CORE->query1('SELECT DISTINCT `year` FROM `basework`');
$years       = $CORE->getQueryResult();
if ( $_POST ){
	$action = '';
	$download = $_POST['download'];
	unset($_POST['download']);
	foreach ($_POST as $key => $value) {
		if ( false !== strpos($key, 'prepRep') ){
			$action = 'prep_report';
			$_POST['prep_report'] = explode('_', $key)[1];
			break;
		}
		if ( $key == 'update' ){
			$action = 'update';
			unset($_POST['update']);
			break;
		}
	}
	reset($_POST);
	if ( $action == 'prep_report' ){
		echo getIndivPlan( $_POST['prep_report'], $_POST['year'], $download );
	} elseif ( $action == 'update' ){
		$content = array();
		foreach ($_POST as $key => $value) {
			$field_id = explode('_', $key);
			$field    = $field_id[0];
			$id       = $field_id[1];
			$content[$id][$field] = $value;
		}
		$CORE->updateTable3('basework',$content);
	} else {

	}
}
if ( ! empty($years) ){
	foreach ($years as $key => $row) {
		$years[$key] = $row['year'];
	}	
}
if ( ! in_array($currentYear, $years) ){
	$years[] = $currentYear;
	$CORE->query1(
		"SELECT `id` FROM `users`
		 WHERE `users`.`role`='teacher'"
	);
	$preps_id = $CORE->getQueryResult();
	if ( empty($preps_id) ){
		return;
	}
	$query = 'INSERT INTO `basework`(`prep_id`,`semester`,`year`) VALUES ';
	for ($i=1; $i < 3; $i++) { 
		foreach ($preps_id as $row) {
			$id = $row['id'];
			$query.= "('$id','$i','$currentYear'), ";
		}
	}
	$query = substr($query, 0, -2);
	$CORE->query1($query);
}
if ( ! empty($pageDetail['params']) && in_array($pageDetail['params'][0], $years)){
	$selectedYear = $pageDetail['params'][0];
}
if ( $selectedYear != '' ){
	if ( array_key_exists(1, $pageDetail['params']) ){
		if ( $pageDetail['params'][1] >= 2 ){
			$selectedSem = 2;
		} elseif ($pageDetail['params'][1] <= 1) {
			$selectedSem = 1;
		}
	} else {
		$selectedSem = 1;
	}
	$fields = array(
		'users'   =>'',
		'basework'=>''
	);
	$ignoreFields = array('semester','year');
	$desc = $CORE->getTableDesc1('basework');
	$dictionary = $CORE->getTable3('dictionary', array( 'where' => array( 'table' => 'basework') ) )['content'];
	if ( ! empty($dictionary) ){
		$newDictionary = array();
		foreach ($dictionary as $key => $value) {
			$newDictionary[$value['word']] = $value['translate'];
		}
		$dictionary = $newDictionary;
		unset($newDictionary);
	}
	foreach ($desc as $key => $value) {
		if ( ! in_array($key, $ignoreFields) ){
			$fields['basework'].= "`basework`.`$key`, ";
		}
	}
	$fields['basework'] = substr($fields['basework'], 0, -2);
	$fields['users'] = '`users`.`name`';
	$query = 'SELECT '.$fields['users'].', '.$fields['basework'].
			 ' FROM `users`, `basework` 
			  WHERE `basework`.`prep_id`=`users`.`id` AND `basework`.`semester`='.$selectedSem.' AND `basework`.`year`='.$selectedYear;
	$CORE->query1($query);
	$table = $CORE->getQueryResult();
	$ignoreFields[] = 'prep_id';
	$ignoreFields[] = 'id';
} 
?>
<p>Год:
<?foreach ($years as $key => $value) {
	if ( $value != $selectedYear ){
		echo "<a href=\"{$pageDetail['path']}/{$value}/\">$value</a>  ";
	} else {
		echo $value.'  ';
	}
}?>
</p>
<?if ( $selectedSem == 1 ){
	echo "<p>Семестр: Первый <a href=\"{$pageDetail['path']}/{$selectedYear}/2/\">Второй</a>";
} elseif ( $selectedSem == 2 ) {
	echo "<p>Семестр: <a href=\"{$pageDetail['path']}/{$selectedYear}/1/\">Первый</a> Второй";
} else {
	return;
}?>
</p>
<br>
<?if ( ! empty($table) ){
	echo "<form method=\"POST\" action=\"{$pageDetail['path']}/$selectedYear/$selectedSem/\"><table class=\"table\"><tr>";
	echo "<td style=\"writing-mode: tb-rl; padding: 5px\">ФИО</td>";
	foreach ($desc as $key => $value) {
		if ( in_array($key, $ignoreFields)){
			continue;
		}
		if ( array_key_exists($key, $dictionary) ){
			$key = $dictionary[$key];
		}
		if (strlen($key) > 30){
			$array30symStr = str_split_unicode($key,30);
			$newStr = array_shift($array30symStr);
			foreach ($array30symStr as $str) {
				$newStr .= '<br>'.$str;
			}
			$key = $newStr;
		}
		echo "<td style=\"writing-mode: tb-rl; padding: 5px\">$key</td>";
	}
	echo "<td style=\"writing-mode: tb-rl; padding: 5px\">ГИП</td>";
	echo '</tr>';
	$constField = array('name');
	foreach ($table as $key => $row) {
		echo '<tr>';
		foreach ($row as $field => $value) {
			if ( in_array($field, $ignoreFields)){
				continue;
			}
			if ( in_array($field, $constField) ){
				echo "<td>$value</td>";
			} else {
				echo "<td><input size=\"$size\" type=\"text\" name=\"{$field}_{$row['id']}\" value=\"$value\"></td>";
			}
		}
		$prep_id = $row['prep_id'];
		echo "<td>
			  		<input type=\"submit\"   name=\"prepRep_{$prep_id}\" value=\"Получить отчёт\">
			  		<input type=\"hidden\"   name=\"year\"               value=\"$selectedYear\">
			  		<input type=\"checkbox\" name=\"download\">
			  </td></tr>";
	}
	echo '</table><input type="submit" name="update" value="Подтвердить изменения"></form>';
}?>
<p>Отчет по всем ИП за <?=$selectedSem?> семестр <?=$selectedYear?> года:
	<input type="submit" name="all"  value="Получить">
	<input type="hidden" name="sem"  value="<?=$selectedSem?>">
	<input type="hidden" name="year" value="<?=$selectedYear?>">
</p>
