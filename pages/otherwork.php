<style>
table.table td {
	border: 2px solid black;
}
</style>
<?
$pageDetail = $CORE->getPageDetail();
if ( empty($pageDetail['params']) ){
	return;
}
$prep_id = $pageDetail['params'][0];
$CORE->query1('SELECT `id` FROM `users` WHERE `users`.`role`=\'teacher\' OR `users`.`role`=\'admin\'');
$users = $CORE->getQueryResult();
$exit = true;
foreach ($users as $key => $row) {
	if ( $row['id'] == $prep_id ){
		$exit = false;
	}
}
if ($exit){
	return;
}
$selectedSem = 0;
if ( array_key_exists(1, $pageDetail['params']) ){
	if ( $pageDetail['params'][1] >= 2 ){
		$selectedSem = 2;
	} elseif ($pageDetail['params'][1] <= 1) {
		$selectedSem = 1;
	}
} else {
	$selectedSem = 1;
}
if ( $selectedSem == 1 ){
	echo "<p>Семестр: Первый <a href=\"{$pageDetail['path']}/{$prep_id}/2/\">Второй</a></p>";
} elseif ( $selectedSem == 2 ) {
	echo "<p>Семестр: <a href=\"{$pageDetail['path']}/{$prep_id}/1/\">Первый</a> Второй</p>";
}
if ( ! empty($_POST) ){
	unset($_POST['update']);
	$load   = array();
	$ignore = array();
	$desc = $CORE->getTableDesc1('otherwork');
	foreach ($_POST as $key => $value) {
		$field_id = explode('|', $key);
		$field = $field_id[0];
		$id = $field_id[1];
		if ( $field == 'detail' && $value == '' ){
			$ignore[] = $id;
			continue;
		}
		$load[$id][$field] = $value;
	}
	foreach ($load as $key => $row) {
		if ( array_key_exists('detail', $row) ){
			$loadCopy = $load;
			foreach ($loadCopy as $ckey => $crow) {
				if ( ! array_key_exists('detail', $crow) || 
				     ($key == $ckey) 
				){
					continue;
				}
				if ( ($row['work_id'] == $crow['work_id']) && 
					 ($row['detail']  == $crow['detail'])
				){
					$load[$key]['detail']  = 0;
				}
			}
			unset($loadCopy);
		}
	}
	reset($load);
	//varDump($load);
	foreach ($load as $key => $row) {
		if ( in_array($key, $ignore) ){
			continue;
		}
		$fields = '';
		foreach ($row as $field => $value) {
			if ( false === strpos($desc[$field]['Type'], 'int') ){
				$value = '\''.$value.'\'';
			} elseif ($value != '') {
				$value = (int)$value;
			} else {
				$value = '\'\'';
			}
			$fields.= "`$field`=$value, ";
		}
		$fields = substr($fields, 0, -2);
		if ( $fields != '' ){
			$CORE->query1("UPDATE `otherwork` SET $fields WHERE `id`={$key};");
			//echo "UPDATE `otherwork` SET $fields WHERE `id`={$key};<br>";
		}
	}
}
$year = date('Y');
$otherwQuery = 'SELECT `id`,`work_id`,`detail`,`workload` FROM `otherwork` WHERE `otherwork`.`prep_id`='.$prep_id.' AND `otherwork`.`semester`='.$selectedSem.' AND `otherwork`.`year`='.$year.';';
$otherwork = $CORE->query1($otherwQuery);

$fillRow = 0;
$rowCount= count($otherwork);
foreach ($otherwork as $key => $row) {
	foreach ($row as $field => $value) {
		if ( $field == 'work_id' && ! empty($value) ){
			$fillRow++;
		}
	}
}
if ( $fillRow == $rowCount ){
	$values = '';
	for ($i=0; $i < 5; $i++) { 
		$values.="($prep_id, '$selectedSem', $year), ";
	}
	$values = substr($values, 0, -2);
	$CORE->query1('INSERT INTO `otherwork`(`prep_id`,`semester`,`year`) VALUES '.$values.';');
	$otherwork = $CORE->query1($otherwQuery);
}
$timerate = $CORE->getTable3('timerate');
foreach ($timerate['content'] as $key => $row) {
	if ( $row['type'] == 'multi') {
		if ( count(explode('_', $row['num'])) != 2){
			continue;
		}
	}
	$work_id[$row['id']] = $row['name'];
}
reset($timerate['content']);
unset($fields);
$fields['work_id'] = array(
	'Type'   => 'ENUM',
	'Default'=> ''
);
$fields['detail'] = array(
	'Type'   => 'ENUM',
	'Default'=> ''
);
$fields['workload'] = array(
	'Type'   => 'INT',
	'Default'=> ''
);
foreach ($otherwork as $key => $row) {
	$id = $row['id'];
	$table[$id] = array(
		'work_id' => $row['work_id'],
		'detail'  => $row['detail'],
		'workload'=> $row['workload']
	);
	foreach ($timerate['content'] as $key => $work) {
		if ( $work['id'] == $row['work_id'] && $work['per'] == 'descipline' ){
			$CORE->query1(
				'SELECT `id`,`name` FROM `descipline`
				WHERE `descipline`.`id` IN
					(SELECT `desc_id` FROM `teacherload`
					WHERE `teacherload`.`prep_id`='.$prep_id.
					');'
			);
			$qres = $CORE->getQueryResult();
			foreach ($qres as $row2) {
				$descipline[$id][$row2['id']] = $row2['name'];
			}
			break;
		}
	}
}
$translate = translateTableFields('otherwork');
?>
<form method="POST" action="<?=$pageDetail['path']."/$prep_id/$selectedSem/"?>">
	<table class="table">
		<tr>
			<?foreach ($fields as $key => $row) {
				if ( array_key_exists($key, $translate) ){
					$string =  $translate[$key];
				} else {
					$string =  $key;
				}
				echo
				"<td>$string</td>";
			}?>
		</tr>
		<?foreach ($table as $key => $row) {
			echo '<tr>';
			foreach ($row as $field => $value) {
				echo '<td>';
				if ( $fields[$field]['Type'] == 'ENUM' ){
					$target = array();
					if ( $field == 'work_id' ){
						$target = $work_id;
					} elseif ( isset($descipline) && array_key_exists($key, $descipline) ){
						$target = $descipline[$key];
					}
					if ( ! empty($target) ){
						echo "<select name=\"{$field}|{$key}\">";
						if ( ! empty($value) && array_key_exists($value, $target) ){
							echo "<option value=\"$value\">{$target[$value]}</option>";
						} else {
							echo '<option value=""></option>';
						}
						foreach ($target as $id => $name) {
							if ($id != $value) {
								echo "<option value=\"$id\">$name</option>";
							}
						}
						if ( ! empty($value) ){
							echo '<option value=""></option>';
						}
						echo '</select>';
					}
				} else {
					echo "<input type=\"text\" name=\"{$field}|{$key}\" value=\"$value\">";
				}
				echo '</td>';
			}
			echo '</tr>';
		}?>
	</table>
	<input type="submit" name="update" value="Отправить">
</form>